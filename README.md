# angular-vernacular-cru-demo

Demo and test project for `angular-vernacular-cru`.

Includes a demo [OpenAPI] definition and rudimentary mock server, and a UI using `angular-vernacular-cru` for that API.

## TL;DR

Serve [OpenAPI] spec: `npm run OpenAPI:serve`

Serve mock backend: `node server`

Serve demo project: `ng serve`

## API

In [OpenAPI], reuse and separations in separate files is possible using YAML `$ref` references in some places. The root
of the specification for each service is `index.yaml`. From here, other files in this repository are referenced directly
and indirectly.

### Development and Visualisation

Although [OpenAPI] is a standard, support for some features differs between tools. We use [ReDoc] as the main target for
our [OpenAPI] spec. [ReDoc] formats and stylizes the specification in-browser.

The tools described here provide continuous feedback on the developer's work (like unit tests).

#### Continuous developer feedback

As a developer, open `index.html` in a browser. This file loads [ReDoc] from a CDN (no dependencies in this repository),
and [ReDoc] loads `index.yaml` and referenced files.

If `index.yaml` contains syntax errors, [ReDoc] shows error messages and warnings.

`index.html` is set up to reload frequently, providing continuous feedback.

#### Linting

Linting is done with [Redocly openapi-cli].

You can execute this test locally with

    > npm install
    > npm test

During development, keep a console open:

    > npm run OpenAPI:watch

This will run the linter each time an OpenAPI file is changed, providing continuous feedback.

On each push, linting is done in [CI].

[openapi]: http://spec.openapis.org/oas/v3.0.3
[ci]: ./bitbucket-pipelines.yml
[redoc]: https://github.com/Redocly/redoc
[redocly openapi-cli]: https://github.com/Redocly/openapi-cli

#### Visualise

To view the [OpenAPI] specification for this demo project, run:

    > npm run OpenAPI:serve

### Mocked API

A mocked API is available in the `server` folder and can be started by invoking the following command from the root of
the project:

```shell
node server
```

## UI

This project demonstrates the capabilities of the `@ppwcode/ng-vernacular-cru` package.

### Development server

After installing the dependencies, you can easily run `ng serve` like you are used to.

> The following steps explain what you need to do when making local changes to the @ppwcode/ng-vernacular-cru repo that
> you'd like to incorporate in this demo project.

#### Vernacular CRU repository

1. Make sure you have cloned [the vernacular CRU repository](https://bitbucket.org/ppwcode/angular-vernacular-cru).
2. Run `ng build` on the vernacula CRU repository (this creates a `dist` folder in that clone of the repo).
3. In the command line, navigate to the `dist/ppwcode/ng-vernacular-cru` folder.
4. Run `npm link`.

> When you make changes to the CRU repository, make sure to delete the `dist` folder before running `ng build` again!

#### This repository

1. Run `npm install`.
2. Run `npm link @ppwcode/ng-vernacular-cru`.
3. Run `ng serve` for the dev server.
4. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use
`ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a
package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the
[Angular CLI Overview and Command Reference](https://angular.io/cli) page.
