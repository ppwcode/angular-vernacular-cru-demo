/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const { persons } = require('./person');
const { stocks } = require('./stock');

/**
 * Searches for one of the given terms in the given array of strings.
 * @param {Array<string>} array
 * @param {Array<string>} terms
 */
function searchForTermsInArray(array, terms) {
    return array.filter((item) => terms.some((term) => item.indexOf(term) > -1));
}

function getResourceFromNameCallback(displayNames, resourcesArray) {
    return (name) => resourcesArray[displayNames.indexOf(name)];
}

function findMatchingItems(displayNames, resourcesArray, terms, type) {
    return searchForTermsInArray(displayNames, terms)
        .map(getResourceFromNameCallback(displayNames, resourcesArray))
        .map((resource) => ({
            type,
            ...resource
        }));
}

function paginate(array, pageSize, pageNumber) {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
}

function getSearchHrefs(terms, pageSize, pageNumber, hasNextPage) {
    return {
        first: `./?terms=${terms.join('%20')}&per_page=${pageSize}&page=1`,
        previous:
            pageNumber > 1 ? `./?terms=${terms.join('%20')}&per_page=${pageSize}&page=${pageNumber - 1}` : undefined,
        next: hasNextPage ? `./?terms=${terms.join('%20')}&per_page=${pageSize}&page=${pageNumber + 1}` : undefined
    };
}

function setUpSearchRouting(app, createError) {
    app.get('/I/search', (req, res) => {
        if (!req.query.terms) {
            createError(400, '`terms` is a required parameter');
        }

        /**
         * @type {Array<string>}
         */
        const terms = (req.query.terms || []).split(' ');
        const pageSize = parseInt(req.query.per_page ?? 20);
        const pageNumber = parseInt(req.query.page ?? 1);

        const lowerTerms = terms.map((t) => t.toLowerCase());
        const lowerPersonNames = persons.map((person) => `${person.firstName} ${person.lastName}`.toLowerCase());
        const lowerStockNames = stocks.map((stock) => stock.symbol.toLowerCase());

        const matchedPersons = findMatchingItems(lowerPersonNames, persons, lowerTerms, '/I/person');
        const matchedStocks = findMatchingItems(lowerStockNames, stocks, lowerTerms, '/I/stock');
        const unpaginatedResults = matchedPersons.concat(...matchedStocks);
        const results = paginate(unpaginatedResults, pageSize, pageNumber);
        const hasNextPage = paginate(unpaginatedResults, pageSize, pageNumber + 1).length > 0;

        res.set('Content-Type', 'application/json');
        res.send(JSON.stringify({ results, href: getSearchHrefs(terms, pageSize, pageNumber, hasNextPage) }));
    });
}

module.exports = { setUpSearchRouting };
