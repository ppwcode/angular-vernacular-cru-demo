/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const { stockExamples, Stock } = require('../schemata/stock/Stock');
const { accountIdExamples } = require('../schemata/id/AccountId');
const moment = require('moment');

function setHateoas(stock) {
    stock.href = {
        history: `${stock.symbol}/history`
    };
    return stock;
}

/**
 * @type {Stock[]}
 */
const stocks = stockExamples.map((s) => setHateoas(s));

const stocksHistory = stockExamples.reduce((val, currentStock) => {
    const historicalInstances = [];

    const amountOfHistoricalInstancesToGenerate = 7;
    const newYear = moment(new Date(2022, 0, 1));
    let createdAt = newYear.clone().add(-7, 'days');
    const intervalBetweenVersions = newYear.diff(createdAt) / (amountOfHistoricalInstancesToGenerate + 1);

    for (let i = 0; i < amountOfHistoricalInstancesToGenerate; i++) {
        createdAt = createdAt.clone();
        historicalInstances.push({
            createdAt,
            href: `${currentStock.symbol}?at=${createdAt.toISOString()}`
        });
        createdAt.add(intervalBetweenVersions, 'milliseconds');
    }

    currentStock.createdAt = newYear.toISOString();
    historicalInstances.push({
        createdAt: currentStock.createdAt,
        href: `${currentStock.symbol}?at=${currentStock.createdAt}`
    });
    currentStock.structureVersion = amountOfHistoricalInstancesToGenerate + 1;

    return {
        ...val,
        [currentStock.symbol]: historicalInstances.reverse()
    };
}, {});

function setUpStockRouting(app, createError) {
    app.param('stockSymbol', function (req, res, next, symbol) {
        req.stock = stocks.find((s) => s.symbol === symbol);
        next();
    });

    app.get('/I/stock', (req, res) => {
        res.set('Content-Type', 'application/json');
        res.send(JSON.stringify(stocks));
    });

    app.put('/I/stock/:stockSymbol', (req, res) => {
        const stockToCreate = req.body;
        let convertedStock;

        if (req.stock) {
            // Update
            const { value: convertedStockToUpdate, error } = Stock.tailor('update').validate(req.body);
            if (error) {
                res.status(400);
                return;
            }

            convertedStockToUpdate.createdAt = new Date().toISOString();
            const index = stocks.indexOf(req.stock);
            stocks[index] = convertedStockToUpdate;
            convertedStock = convertedStockToUpdate;
            stocksHistory[convertedStockToUpdate.symbol].unshift({
                createdAt: convertedStockToUpdate.createdAt,
                href: `${convertedStockToUpdate.symbol}?at=${convertedStockToUpdate.createdAt}`
            });
        } else {
            // Create
            const { value: convertedStockToCreate, error } = Stock.tailor('create').validate(stockToCreate);
            if (error) {
                res.status(400);
                return;
            }

            convertedStockToCreate.createdAt = new Date().toISOString();
            convertedStockToCreate.createdBy = accountIdExamples[0];
            const stockWithHateoas = setHateoas(convertedStockToCreate);
            stocks.push(stockWithHateoas);
            stocksHistory[stockWithHateoas.symbol] = [
                {
                    createdAt: stockWithHateoas.createdAt,
                    href: `${stockWithHateoas.symbol}?at=${stockWithHateoas.createdAt}`
                }
            ];
            convertedStock = convertedStockToCreate;
        }

        res.set('Content-Type', 'application/json');
        res.status(200);
        res.send(convertedStock);
    });

    app.get('/I/stock/:stockSymbol/history', (req, res) => {
        res.set('Content-Type', 'application/json');
        const historicVersions = stocksHistory[req.stock.symbol] ?? [];
        res.send({
            structureVersion: historicVersions.length,
            versions: historicVersions
        });
    });

    app.get('/I/stock/:stockSymbol', (req, res) => {
        res.set('Content-Type', 'application/json');
        if (req.query.at) {
            req.stock = { ...req.stock };
            req.stock.createdAt = req.query.at;
        }
        req.stock.extraProperty = 'The client should ignore extra properties';
        res.send(req.stock);
    });
}

module.exports = { setUpStockRouting, stocks };
