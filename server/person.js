/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const { personExamples, Person } = require('../schemata/person/Person');
const { accountIdExamples } = require('../schemata/id/AccountId');
const moment = require('moment');
const { notUndefined } = require('@ppwcode/js-ts-oddsandends/lib/conditional-assert');

let personId = 0;

function setHateoas(person) {
    person.href = {
        history: `${person.personId}/history`
    };
    return person;
}

const persons = personExamples.map((p) => {
    p.personId = ++personId;
    return setHateoas(p);
});

const personsHistory = personExamples.reduce((val, currentPerson) => {
    const historicalInstances = [];

    const amountOfHistoricalInstancesToGenerate = 7;
    const newYear = moment(new Date(2022, 0, 1));
    let createdAt = newYear.clone().add(-7, 'days');
    const intervalBetweenVersions = newYear.diff(createdAt) / (amountOfHistoricalInstancesToGenerate + 1);

    for (let i = 0; i < amountOfHistoricalInstancesToGenerate; i++) {
        createdAt = createdAt.clone();
        historicalInstances.push({
            createdAt,
            href: `${currentPerson.personId}?at=${createdAt.toISOString()}`
        });
        createdAt.add(intervalBetweenVersions, 'milliseconds');
    }

    currentPerson.createdAt = newYear.toISOString();
    historicalInstances.push({
        createdAt: currentPerson.createdAt,
        href: `${currentPerson.personId}?at=${currentPerson.createdAt}`
    });
    currentPerson.structureVersion = amountOfHistoricalInstancesToGenerate + 1;

    const id = notUndefined(currentPerson.personId).toString();
    return {
        ...val,
        [id]: historicalInstances.reverse()
    };
}, {});

function setUpPersonRouting(app, createError) {
    app.param('personId', function (req, res, next, id) {
        if ((req.person = persons.find((p) => p.personId === parseInt(id)))) {
            next();
        } else {
            next(createError(404, 'Failed to find person'));
        }
    });

    app.get('/I/person', (req, res) => {
        res.set('Content-Type', 'application/json');
        res.send(JSON.stringify(persons));
    });

    app.post('/I/person', (req, res) => {
        const personToCreate = req.body;
        const { value: convertedPerson, error } = Person.tailor('create').validate(personToCreate);
        if (error) {
            res.status(400);
            return;
        }

        convertedPerson.personId = ++personId;
        convertedPerson.createdAt = new Date().toISOString();
        convertedPerson.createdBy = accountIdExamples[0];
        persons.push(setHateoas(convertedPerson));
        personsHistory[convertedPerson.personId] = [
            {
                createdAt: convertedPerson.createdAt,
                href: `${convertedPerson.personId}?at=${convertedPerson.createdAt}`
            }
        ];

        res.set('Content-Type', 'application/json');
        res.status(201);
        res.send();
    });

    app.get('/I/person/:personId/history', (req, res) => {
        res.set('Content-Type', 'application/json');
        const historicVersions = personsHistory[req.person.personId];
        res.send({
            structureVersion: historicVersions.length,
            versions: historicVersions
        });
    });

    app.get('/I/person/:personId', (req, res) => {
        res.set('Content-Type', 'application/json');
        if (req.query.at) {
            req.person = { ...req.person };
            req.person.createdAt = req.query.at;
        }
        req.person.extraProperty = 'The client should ignore extra properties';
        setTimeout(() => {
            res.send(req.person);
        }, 1000);
    });

    app.put('/I/person/:personId', (req, res) => {
        const { value: convertedPerson, error } = Person.tailor('update').validate(req.body);
        if (error) {
            res.status(400);
            return;
        }

        convertedPerson.createdAt = new Date().toISOString();
        const index = persons.indexOf(req.person);
        persons[index] = convertedPerson;
        personsHistory[convertedPerson.personId].unshift({
            createdAt: convertedPerson.createdAt,
            href: `${convertedPerson.personId}?at=${convertedPerson.createdAt}`
        });

        res.status(200);
        setTimeout(() => {
            res.send(convertedPerson);
        }, 1000);
    });
}

module.exports = { setUpPersonRouting, persons };
