/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../../_util/addExamples');

const SearchResultBase = Joi.object({
    type: Joi.string().uri({ relativeOnly: true }).required()
        .description(`Type of the resource that matches the search, expressed as a partial URI. This must be the start of
the \`href\` property value.`),
    href: Joi.string().uri({ relativeOnly: true }).required()
        .description(`Server relative URI where the found resource's detail can be retrieved. The \`at\` parameter makes clear what
      iteration of the resource was indexed most recently. This must start with the \`type\` property value.`)
}).unknown(true);

const examples = [{ type: '/I/person', href: '/I/person/56349?at=2021-01-19T17:14:18.482Z' }];

module.exports = { searchResultBaseExamples: examples, SearchResultBase: addExamples(SearchResultBase, examples) };
