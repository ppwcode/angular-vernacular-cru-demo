/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../../_util/addExamples');
const { SearchResultBase } = require('./SearchResultBase');
const { stockSearchDocumentExamples, StockSearchDocument } = require('../../stock/search/StockSearchDocument');

const StockSearchResult = SearchResultBase.concat(StockSearchDocument).append({
    type: Joi.valid('/I/stock').description('discriminator')
});

const examples = stockSearchDocumentExamples.map((ssd) => ({
    type: '/I/stock',
    ...ssd,
    href: '/I/stock/KLJHS?at=2021-01-19T17:14:18.482Z'
}));

module.exports = { stockSearchResultExamples: examples, StockSearchResult: addExamples(StockSearchResult, examples) };
