/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../_util/addExamples');
const { PersonSearchResult, personSearchResultExamples } = require('./result/PersonSearchResult');
const { StockSearchResult, stockSearchResultExamples } = require('./result/StockSearchResult');
const { StructureVersioned, structureVersionedExamples } = require('../resource/StructureVersioned');
const { SearchResult, searchResultExamples } = require('./SearchResult');

const URI = Joi.string().uri({ relativeOnly: true });

const SearchResultsHref = Joi.object({
    first: URI.required().description(`Relative URI of the first page. The \`terms\` query parameter has the same
value as in the request.`),
    previous: URI.optional()
        .description(`Relative URI of previous next page. Not present when we are at the first page. The \`terms\` query
parameter has the same value as in the request.`),
    next: URI.optional()
        .description(`Relative URI of the next page. Not present when we are at the last page. The \`terms\` query
parameter has the same value as in the request.`)
});

const searchResultsHrefExamples = [
    {
        first: './?terms=Anna%20Van%20Deuren%20Arendonk%201912-05-01&per_page=20&page=1',
        previous: './?terms=Anna%20Van%20Deuren%20Arendonk%201912-05-01&per_page=20&page=4',
        next: './?terms=Anna%20Van%20Deuren%20Arendonk%201912-05-01&per_page=20&page=6'
    }
];

const SearchResults = StructureVersioned.append({
    results: Joi.array().items(SearchResult.required()).max(100).required()
        .description(`Sequence of search results, in descending order of relevance.


Elements are of different types, and hold values that identify the found resource for the user, and a \`href\`
where the indexed version of the resource can be downloaded.


The type of the matching resource is indicated by the discriminator \`type\`.`),
    href: SearchResultsHref.required().description('links to other pages in this sequence')
});

const examples = structureVersionedExamples.map((svd) => ({
    ...svd,
    results: searchResultExamples,
    href: searchResultsHrefExamples[0]
}));

module.exports = { searchResultsExamples: examples, SearchResults: addExamples(SearchResults, examples) };
