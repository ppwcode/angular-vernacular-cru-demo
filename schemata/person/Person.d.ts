/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import * as Joi from 'joi';
import { INSS } from '../id/naturalPerson/be/INSS';
import { Gender } from '../human/Gender';
import { Address } from '../location/Address';
import { Language } from '../string/Language';
import { TrimmedString } from '../string/TrimmedString';
import { DayDate } from '../time/DayDate';
import { Sourced } from './Sourced';

export declare type PersonHref = {
    history: string;
};

export declare interface Person extends Sourced {
    personId?: number;
    inss: INSS;
    firstName: TrimmedString;
    lastName: TrimmedString;
    dateOfBirth: DayDate;
    pronouns: Gender;
    language: Language;
    address: Address;
    emails: Array<string>;
    href: PersonHref;
}

export declare const Person: Joi.ObjectSchema<Person>;

export declare const personExamples: Array<Person>;
