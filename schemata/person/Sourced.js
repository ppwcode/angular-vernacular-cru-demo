/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const { Audited, auditedExamples } = require('../resource/Audited');
const addExamples = require('../_util/addExamples');
const { structureVersionExamples } = require('../resource/StructureVersion');
const { Source, sourceExamples } = require('./Source');
const { StructureVersioned } = require('../resource/StructureVersioned');

const Sourced = Audited.concat(StructureVersioned).keys({
    source: Source.optional()
});

const sourcedExamples = auditedExamples.reduce(
    (acc1, audited) =>
        structureVersionExamples.reduce(
            (acc2, structureVersion) =>
                sourceExamples.concat([undefined]).reduce((acc3, source) => {
                    acc2.push({ structureVersion, ...audited, source });
                    return acc3;
                }, acc2),
            acc1
        ),
    []
);

module.exports = {
    sourcedExamples,
    Sourced: addExamples(Sourced, sourcedExamples)
};
