/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const { Sourced, sourcedExamples } = require('./Sourced');
const { readOnlyAlteration } = require('../resource/_readOnlyAlteration');
const addExamples = require('../_util/addExamples');
const { INSS, inssExamples } = require('../id/naturalPerson/be/INSS');
const { TrimmedString } = require('../string/TrimmedString');
const { Gender, genderExamples } = require('../human/Gender');
const { Language, languageExamples } = require('../string/Language');
const { Address, addressExamples } = require('../location/Address');
const { DayDate } = require('../time/DayDate');

const Person = Sourced.append({
    personId: Joi.number().required().description('the unique identification of the person on the server'),
    inss: INSS.required(),
    firstName: TrimmedString.required().description(
        'first name or names of the person, relevant for identification and addressing the person'
    ),
    lastName: TrimmedString.required().description('last name of the person; relevant for identification'),
    dateOfBirth: DayDate.required(),
    pronouns: Gender.description('the gender the person prefers we use in communication').required(),
    language: Language.required(),
    address: Address.required().description('address where the person can be reached for communication'),
    emails: Joi.array()
        .items(
            Joi.string()
                // TLDs are disabled because we would get an error: "Built-in TLD list disabled"
                // https://github.com/sideway/joi/issues/2037 “New Features” “Official browser support”
                .email({ tlds: { allow: false } })
                .required()
        )
        .unique()
        .required()
        .description('emails where the person can be reached for communication'),
    href: Joi.object({
        history: Joi.string()
            .uri({ relativeOnly: true })
            .min(1)
            .required()
            .description(
                "relative URI at which the list of dates at which a change in the person's personal information was changed, can be retrieved"
            )
    })
        .alter(readOnlyAlteration)
        .description('references to related resources')
});

const personExamples = sourcedExamples.map((sourced) => ({
    ...sourced,
    personId: 63663,
    inss: inssExamples[0],
    firstName: 'Jane',
    lastName: 'Doe',
    dateOfBirth: '1978-12-14',
    pronouns: genderExamples[0],
    language: languageExamples[0],
    address: addressExamples[0],
    emails: ['jane.doe@example.org'],
    href: { history: '63663/history' }
}));

module.exports = { personExamples, Person: addExamples(Person, personExamples) };
