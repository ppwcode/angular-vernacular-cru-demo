/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../../_util/addExamples');
const { SearchDocumentBase, searchDocumentBaseExamples } = require('../../resource/SearchDocumentBase');
const { Address } = require('../../location/Address');
const { Person } = require('../Person');
const { INSS } = require('../../id/naturalPerson/be/INSS');

const PersonSearchDocument = SearchDocumentBase.append({
    insses: Joi.array().items(INSS.required()).required()
        .description(`Sequence of INSS-values of the represented person, ordered from most recent to first
known.


Persons have an INSS at every point in there existence in the system, but it can change over time. All INSSes
we have ever known for the person are indexed. The one we should use currently is the first.`),
    firstName: Person.extract('firstName'),
    lastName: Person.extract('lastName'),
    postalCode: Address.extract('postalCode'),
    municipality: Address.extract('municipality'),
    emails: Joi.array()
        .items(Joi.string().email({ tlds: false }).required())
        .required()
        .description(`The email addresses the person is believed to have at \`createdAt\`. Unordered.`)
});

const personSearchDocumentExamples = searchDocumentBaseExamples.map((sdb) => ({
    ...sdb,
    insses: ['34010143459', '88120484855'],
    firstName: 'Maria',
    lastName: 'Van Dael',
    postalCode: 'B-2300',
    municipality: 'Turnhout',
    emails: ['mvd@example.org', 'maria@heaven.org']
}));

module.exports = {
    personSearchDocumentExamples,
    PersonSearchDocument: addExamples(PersonSearchDocument, personSearchDocumentExamples)
};
