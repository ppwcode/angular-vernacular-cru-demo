/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../_util/addExamples');

const Source = Joi.string().min(1).trim()
    .description(`Reference to the source of this version of this information. The semantics is opaque to this system.


It should be used at the discretion of human or machine clients to make clear what the authoritative source of the
information is. This value extends the semantics of \`createdBy\` in the audit trail.


To be used by import programs that crunch bulk import files, to precisely refer to the file from which this version of
this information originated.`);

const sourceExamples = ['import file ABC 2021-02'];

module.exports = { sourceExamples, Source: addExamples(Source, sourceExamples) };
