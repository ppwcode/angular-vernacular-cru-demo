/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../../_util/addExamples');
const { SearchDocumentBase, searchDocumentBaseExamples } = require('../../resource/SearchDocumentBase');
const { Address } = require('../../location/Address');
const { Stock } = require('../Stock');

const StockSearchDocument = SearchDocumentBase.append({
    symbol: Joi.string()
        .trim()
        .uppercase()
        .required()
        .description('symbolic representation of the stock; unique and immutable'),
    name: Stock.extract('name'),
    postalCode: Address.extract('postalCode'),
    municipality: Address.extract('municipality')
});

const stockSearchDocumentExamples = searchDocumentBaseExamples.map((svd) => ({
    ...svd,
    symbol: 'JLFG',
    name: 'Acme',
    postalCode: '2300AD',
    municipality: 'Turnhout'
}));

module.exports = {
    stockSearchDocumentExamples,
    StockSearchDocument: addExamples(StockSearchDocument, stockSearchDocumentExamples)
};
