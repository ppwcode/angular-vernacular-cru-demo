/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi');
const addExamples = require('../_util/addExamples');
const { TrimmedString } = require('../string/TrimmedString');
const { Address, addressExamples } = require('../location/Address');
const { Audited, auditedExamples } = require('../resource/Audited');
const { readOnlyAlteration } = require('../resource/_readOnlyAlteration');
const { StructureVersioned } = require('../resource/StructureVersioned');

const Stock = Audited.concat(StructureVersioned).append({
    name: TrimmedString.required().description('name of the stock'),
    address: Address.required().description('address where the stock is legally addressable'),
    href: Joi.object({
        history: Joi.string()
            .uri({ relativeOnly: true })
            .min(1)
            .required()
            .description(
                "relative URI at which the list of dates at which a change in the resource's information was changed, can be retrieved"
            )
    })
        .alter(readOnlyAlteration)
        .description('references to related resources')
});

const stockExamples = auditedExamples.map((audited) => ({
    ...audited,
    structureVersion: 1,
    symbol: 'ACM',
    name: 'Acme',
    address: addressExamples[0],
    href: { history: 'KJHLGF/history' }
}));

module.exports = { stockExamples, Stock: addExamples(Stock, stockExamples) };
