/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
    createSaveCancelViewSuite,
    ExpectedFormControlErrors,
    viewEditSaveCancelViewSuite
} from '../common/suites/cru';

describe('Stock', () => {
    const expectedFormControlErrors: ExpectedFormControlErrors = {
        name: {
            'Dit veld mag niet leeg zijn.': ['']
        },
        'address.line1': {
            'Dit veld mag niet leeg zijn.': ['']
        },
        'address.municipality': {
            'Dit veld mag niet leeg zijn.': ['']
        },
        'address.country': {
            'Dit veld mag niet leeg zijn.': ['']
        }
    };

    createSaveCancelViewSuite({
        createResourceControls: [
            'name',
            'symbol',
            'address.postalCode',
            'address.line1',
            'address.municipality',
            'address.country'
        ],
        createValues: {
            name: 'Acme Corp',
            symbol: 'Acme',
            'address.line1': 'Street 16',
            'address.municipality': 'Awesome town',
            'address.postalCode': '1000',
            'address.country': 'BE'
        },
        readResourceControls: ['name', 'address.line1', 'address.municipality', 'address.country'],
        readValues: {
            name: 'Acme Corp',
            'address.line1': 'Street 16',
            'address.municipality': 'Awesome town',
            'address.country': 'BE'
        },
        initialFormValue: {},
        createAppRoute: '#/stocks/create',
        resourceAppRoute: '#/stocks/Acme/read',
        createComponentSelector: 'ppwcode-cru-demo-stock-create',
        expectedFormControlErrors: {
            ...expectedFormControlErrors,
            'address.postalCode': {
                'Dit veld mag niet leeg zijn.': ['']
            }
        }
    });

    viewEditSaveCancelViewSuite({
        allResourceControls: ['name', 'address.line1', 'address.municipality', 'address.country'],
        changeValues: { name: 'Acme Corp.' },
        initialFormValue: {
            name: 'Acme'
        },
        readComponentSelector: 'ppwcode-cru-demo-stock-read',
        updateComponentSelector: 'ppwcode-cru-demo-stock-update',
        resourceApiUrl: '/api/I/stock/ACM',
        resourceAppRoute: '#/stocks/ACM/read',
        expectedFormControlErrors
    });
});
