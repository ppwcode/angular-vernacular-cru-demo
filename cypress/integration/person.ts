/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { ExpectedFormControlErrors, viewEditSaveCancelViewSuite } from '../common/suites/cru';

const alphabetLowerCase: Array<string> = [...'abcdefghijklmnopqrstuvwxyz'];
const alphabetUpperCase: Array<string> = alphabetLowerCase.map((char: string) => char.toUpperCase());

describe('Person', () => {
    const expectedFormControlErrors: ExpectedFormControlErrors = {
        inss: {
            'Dit veld mag niet leeg zijn.': [''],
            // TODO: This pattern should be /^\d{11}$/.
            'Dit veld voldoet niet aan het patroon /\\d{11}/.': [
                '1',
                '12',
                '123',
                '1234',
                '12345',
                '123456',
                '1234567',
                '12345678',
                '123456789',
                '1234567890',
                // '123456789012' should also be invalid because of its length
                '1234567890a'
            ]
        },
        dateOfBirth: {
            'Dit veld mag niet leeg zijn.': ['']
        },
        pronouns: {
            'Dit veld moet een van volgende waardes bevatten: F,M,X.': [
                '',
                ...alphabetLowerCase,
                ...alphabetUpperCase.filter((char: string) => ['F', 'M', 'X'].indexOf(char) === -1)
            ]
        },
        language: {
            'Dit veld mag niet leeg zijn.': [''],
            'Dit veld voldoet niet aan het patroon /^[a-z]{2}(-[A-Z]{2})?$/.': ['b', 'bb-', 'bb-d', 'bb-dd', 'B', 'bB']
        }
    };

    viewEditSaveCancelViewSuite({
        allResourceControls: ['inss', 'dateOfBirth', 'pronouns', 'language'],
        changeValues: { pronouns: 'M' },
        initialFormValue: {
            inss: '86111201234',
            dateOfBirth: '1978-12-14',
            pronouns: 'F',
            language: 'fr-BE'
        },
        readComponentSelector: 'ppwcode-cru-demo-person-read',
        updateComponentSelector: 'ppwcode-cru-demo-person-update',
        resourceApiUrl: '/api/I/person/1',
        resourceAppRoute: '#/persons/1/read',
        expectedFormControlErrors
    });
});
