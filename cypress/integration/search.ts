import { clearText, enterText } from '../common/forms/input';
import { clickNthItem } from '../common/mouse';
import { appShouldBeAtRoute } from '../common/routing/path';
import { shouldHaveLength } from '../common/sizes';
import Chainable = Cypress.Chainable;

export const searchInputSelector: string = '[data-cy=searchInput]';
export const searchResultsSelector: string = '.search-result';

describe('Search', () => {
    const getClearSearchButton = (): Chainable =>
        cy.get(searchInputSelector).parent().parent().get('.mat-form-field-suffix mat-icon');

    beforeEach(() => {
        cy.visit('/');
    });

    it('should load the search results and be able to navigate to them', () => {
        shouldHaveLength(searchResultsSelector, 0);

        enterText(searchInputSelector, 'a');
        shouldHaveLength(searchResultsSelector, 3);

        clickNthItem(searchResultsSelector, 0);
        appShouldBeAtRoute('#/persons/1/read');

        clickNthItem(searchResultsSelector, 2);
        appShouldBeAtRoute('#/stocks/ACM/read');

        getClearSearchButton().click();
        enterText(searchInputSelector, 'an');
        shouldHaveLength(searchResultsSelector, 2);

        getClearSearchButton().click();
        enterText(searchInputSelector, 'and');
        shouldHaveLength(searchResultsSelector, 0);

        clearText(searchInputSelector);
        shouldHaveLength(searchResultsSelector, 0);
    });
});
