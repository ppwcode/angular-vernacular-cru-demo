import { appShouldBeAtRoute } from '../common/routing/path';

describe('Home', () => {
    it('should display the home page when navigating to the root of the application', () => {
        cy.visit('/');
        appShouldBeAtRoute('#/home');
    });
});
