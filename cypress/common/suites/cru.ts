/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { clickTimestamp, selectHistoricVersion } from '../data/history';
import { toControlSelectors } from '../forms/controls';
import {
    allFormFieldsShouldBeDisabled,
    allFormFieldsShouldBeEnabled,
    allFormFieldsShouldBeInvalidForInput,
    allFormFieldsShouldBeValid
} from '../forms/form-field';
import { formShouldHaveValues, setFormFieldValues } from '../forms/values';
import { cancelChanges, goToEdit, saveChanges } from '../routing/cru';
import { appShouldBeAtRoute } from '../routing/path';

/**
 * The type for expected single control errors. The key is the actual error and the value an array of text values that
 * should raise the error.
 */
export declare type ControlErrors = Record<string, Array<string>>;

/**
 * The type that combines the expected control errors for a full form. The key is the name of the form control.
 */
export declare type ExpectedFormControlErrors = Record<string, ControlErrors>;

/**
 * This interface describes the parameters to define the behavior of the createSaveCancelView test suite.
 */
export interface CreateSaveCancelViewSuiteOptions {
    /** The names of all the form controls for creating the resource. */
    createResourceControls: Array<keyof this['expectedFormControlErrors']>;
    /** The values that should be used when in edit mode. */
    createValues: Record<string, string>;
    /** The names of all the form controls for displaying the resource. */
    readResourceControls: Array<string>;
    /** The values that should be visible when in read mode. */
    readValues: Record<string, string>;
    /** The initially expected form value. Used to reset the resource again at the end of the suite. */
    initialFormValue: Record<string, string>;
    /** The selector to find the create component of the resource. */
    createComponentSelector: string;
    /** The route of the create page to create a new instance. */
    createAppRoute: string;
    /** The route of the SUT in the application. */
    resourceAppRoute: string;
    /** Expected errors on the given form controls for the given values. */
    expectedFormControlErrors: ExpectedFormControlErrors;
}

/**
 * This interface describes the parameters to define the behavior of the viewEditSaveCancelView test suite.
 */
export interface ViewEditSaveCancelViewSuiteOptions {
    /** The names of all the form controls for the resource. */
    allResourceControls: Array<keyof this['expectedFormControlErrors']>;
    /** The values that should be changed when in edit mode. */
    changeValues: Record<string, string>;
    /** The initially expected form value. Used to reset the resource again at the end of the suite. */
    initialFormValue: Record<string, string>;
    /** The selector to find the read component of the resource. */
    readComponentSelector: string;
    /** The route of the SUT in the application. */
    resourceAppRoute: string;
    /** The url of the endpoint that will be called to get the resource by its identifier. */
    resourceApiUrl: string;
    /** The selector to find the update component of the resource. */
    updateComponentSelector: string;
    /** Expected errors on the given form controls for the given values. */
    expectedFormControlErrors: ExpectedFormControlErrors;
}

/**
 * Test suite that verifies the following flow.
 * - Create: A resource is being created.
 * - Save/Cancel: The values for the resource are saved or aborted.
 * - View: The resource is in readonly mode with persisted changes.
 * @param options The options to direct the behavior of the suite.
 */
export const createSaveCancelViewSuite = (options: CreateSaveCancelViewSuiteOptions): void => {
    const {
        createResourceControls,
        createValues,
        readResourceControls,
        readValues,
        initialFormValue,
        createComponentSelector,
        createAppRoute,
        resourceAppRoute,
        expectedFormControlErrors
    }: CreateSaveCancelViewSuiteOptions = options;

    const createControlSelectors: Record<string, string> = toControlSelectors(createResourceControls);
    const readControlSelectors: Record<string, string> = toControlSelectors(readResourceControls);

    it('should have a Create - Save/Cancel - View cycle', () => {
        // Navigate to the create route.
        cy.visit(createAppRoute);
        allFormFieldsShouldBeEnabled(createControlSelectors);
        formShouldHaveValues(initialFormValue, createControlSelectors);
        allFormFieldsShouldBeInvalidForInput(createControlSelectors, expectedFormControlErrors);

        // Cancel out.
        cancelChanges(createComponentSelector);
        appShouldBeAtRoute('/home');

        // Make changes and save.
        cy.visit(createAppRoute);
        allFormFieldsShouldBeEnabled(createControlSelectors);

        setFormFieldValues(createValues, createControlSelectors);
        formShouldHaveValues({ ...initialFormValue, ...createValues }, createControlSelectors);

        saveChanges(createComponentSelector);
        appShouldBeAtRoute(resourceAppRoute);
        allFormFieldsShouldBeDisabled(readControlSelectors);
        formShouldHaveValues({ ...initialFormValue, ...readValues }, readControlSelectors);
    });
};

/**
 * Test suite that verifies the following flow.
 * - View: A resource is being opened.
 * - Edit: The resource is put into edit mode.
 * - Save/Cancel: The changes to a resource are saved or aborted.
 * - View: The resource is in readonly mode again with persisted changes or no changes at all depending on the Save/Cancel.
 * @param options The options to direct the behavior of the suite.
 */
export const viewEditSaveCancelViewSuite = (options: ViewEditSaveCancelViewSuiteOptions): void => {
    const {
        allResourceControls,
        changeValues,
        initialFormValue,
        readComponentSelector,
        resourceAppRoute,
        resourceApiUrl,
        updateComponentSelector,
        expectedFormControlErrors
    }: ViewEditSaveCancelViewSuiteOptions = options;

    const allControlSelectors: Record<string, string> = toControlSelectors(allResourceControls);

    it('should have a View - Edit - Save/Cancel - View cycle', () => {
        const initialValuesThatWillBeChanged: Record<string, string> = Object.keys(changeValues).reduce(
            (val: Record<string, string>, controlName: string) => ({
                ...val,
                [controlName]: notUndefined(initialFormValue[controlName])
            }),
            {}
        );

        // Navigate to the person.
        cy.visit(resourceAppRoute);
        allFormFieldsShouldBeDisabled(allControlSelectors);
        allFormFieldsShouldBeValid(allControlSelectors);
        formShouldHaveValues(initialFormValue, allControlSelectors);

        clickTimestamp();
        selectHistoricVersion(1);
        cy.wait(1000); // It takes a second before the resource is loaded.
        appShouldBeAtRoute(`${resourceAppRoute}?at=2021-12-31T02:00:00.000Z`);

        clickTimestamp();
        selectHistoricVersion(0);
        appShouldBeAtRoute(`${resourceAppRoute}?at=2021-12-31T23:00:00.000Z`);

        // Open edit mode but cancel out (we use this stage as well to verify any input validation errors that should appear). */
        cy.intercept(resourceApiUrl).as('getResource');
        goToEdit(readComponentSelector);
        cy.wait('@getResource');
        allFormFieldsShouldBeValid(allControlSelectors);
        allFormFieldsShouldBeEnabled(allControlSelectors);
        allFormFieldsShouldBeInvalidForInput(allControlSelectors, expectedFormControlErrors);

        cancelChanges(updateComponentSelector);
        allFormFieldsShouldBeDisabled(allControlSelectors);
        allFormFieldsShouldBeValid(allControlSelectors);
        formShouldHaveValues(initialFormValue, allControlSelectors);

        // Open edit mode, make changes and save.
        cy.intercept(resourceApiUrl).as('getResource2');
        goToEdit(readComponentSelector);
        cy.wait('@getResource2');
        allFormFieldsShouldBeValid(allControlSelectors);
        allFormFieldsShouldBeEnabled(allControlSelectors);

        setFormFieldValues(changeValues, allControlSelectors);
        allFormFieldsShouldBeValid(allControlSelectors);
        formShouldHaveValues({ ...initialFormValue, ...changeValues }, allControlSelectors);

        saveChanges(updateComponentSelector);
        allFormFieldsShouldBeDisabled(allControlSelectors);
        allFormFieldsShouldBeValid(allControlSelectors);
        formShouldHaveValues({ ...initialFormValue, ...changeValues }, allControlSelectors);

        // Clean up again so that other tests have the same starting point.
        cy.intercept(resourceApiUrl).as('getResource3');
        goToEdit(readComponentSelector);
        cy.wait('@getResource3');
        setFormFieldValues(initialValuesThatWillBeChanged, allControlSelectors);
        saveChanges(updateComponentSelector);
        formShouldHaveValues(initialFormValue, allControlSelectors);
    });
};
