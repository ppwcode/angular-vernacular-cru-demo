/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

export const DEFAULT_EDIT_ICON: string = 'edit';
export const DEFAULT_CANCEL_ICON: string = 'close';
export const DEFAULT_SAVE_ICON: string = 'save';

/**
 * Navigates to the edit page by clicking the icon within the given component selector of the read component.
 * @param componentSelector The selector to get the read component.
 */
export const goToEdit = (componentSelector: string): void => {
    cy.get(`${componentSelector} mat-icon`).contains(DEFAULT_EDIT_ICON).click();
};

/**
 * Cancels any changes that have been made by clicking the icon within the given component selector of the edit component.
 * @param componentSelector The selector to get the update component.
 */
export const cancelChanges = (componentSelector: string): void => {
    cy.get(`${componentSelector} mat-icon`).contains(DEFAULT_CANCEL_ICON).click();
};

/**
 * Saves any changes that have been made by clicking the icon within the given component selector of the edit component.
 * @param componentSelector The selector to get the update component.
 */
export const saveChanges = (componentSelector: string): void => {
    cy.get(`${componentSelector} mat-icon`).contains(DEFAULT_SAVE_ICON).click();
};
