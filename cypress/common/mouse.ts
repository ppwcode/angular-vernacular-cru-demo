/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Clicks the nth occurrence of an element that matches the given selector.
 * If no index is passed to this function, it just clicks whatever the selector finds.
 * @param selector The selector to get the elements for.
 * @param itemIndex The index of the item in the results to click.
 */
export const clickNthItem = (selector: string, itemIndex: number | null = null): void => {
    if (itemIndex === null) {
        cy.get(selector).click();
    } else {
        cy.get(selector).eq(itemIndex).click();
    }
};
