/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Gets the control selector for the given control name.
 * @param controlName The name of the form control that is being used internally.
 */
export const toControlSelector = (controlName: string): string => `[data-control-name="${controlName}"]`;

/**
 * Gets the control selectors for the given array of control names. Returns an object with the control name as the key
 * and its corresponding selector as the value.
 * @param controlNames The name of the form controls that are being used internally.
 */
export const toControlSelectors = (controlNames: Array<string>): Record<string, string> =>
    controlNames.reduce(
        (selectors: Record<string, string>, controlName: string) => ({
            ...selectors,
            [controlName]: toControlSelector(controlName)
        }),
        {}
    );
