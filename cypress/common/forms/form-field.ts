/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { ExpectedFormControlErrors } from '../suites/cru';
import { setFormFieldValues } from './values';

/**
 * Asserts that the mat-form-field within the given selector is disabled by checking whether the mat-form-field-disabled
 * class is set.
 * @param selector The selector for the form field.
 */
export const formFieldShouldBeDisabled = (selector: string): void => {
    cy.get(selector + ' mat-form-field').should('have.class', 'mat-form-field-disabled');
};

/**
 * Asserts that the mat-form-field within the given selectors is disabled by checking whether the mat-form-field-disabled
 * class is set.
 * @param selectors An object where the key is the control name and the value the control selector.
 */
export const allFormFieldsShouldBeDisabled = (selectors: Record<string, string>): void => {
    Object.keys(selectors).forEach((controlName: string) => {
        const controlSelector: string = notUndefined(selectors[controlName]);
        formFieldShouldBeDisabled(controlSelector);
    });
};

/**
 * Asserts that the mat-form-field within the given selector is enabled by checking whether the mat-form-field-disabled
 * class is not set.
 * @param selector The selector for the form field.
 */
export const formFieldShouldBeEnabled = (selector: string): void => {
    cy.get(selector + ' mat-form-field').should('not.have.class', 'mat-form-field-disabled');
};

/**
 * Asserts that the mat-form-field within the given selectors is enabled by checking whether the mat-form-field-disabled
 * class is not set.
 * @param selectors An object where the key is the control name and the value the control selector.
 */
export const allFormFieldsShouldBeEnabled = (selectors: Record<string, string>): void => {
    Object.keys(selectors).forEach((controlName: string) => {
        const controlSelector: string = notUndefined(selectors[controlName]);
        formFieldShouldBeEnabled(controlSelector);
    });
};

/**
 * Asserts that there is no mat-error instance within the given selector.
 * @param selector The selector to locate the form control.
 */
export const formFieldShouldBeValid = (selector: string): void => {
    cy.get(selector + ' mat-error').should('not.exist');
};

/**
 * Asserts that there is no mat-error instance for the given selectors.
 * @param selectors The selectors to locate the form controls.
 */
export const allFormFieldsShouldBeValid = (selectors: Record<string, string>): void => {
    Object.values(selectors).forEach((selector: string) => {
        formFieldShouldBeValid(selector);
    });
};

/**
 * Asserts that there is a mat-error instance with the given text within the given selector.
 * @param selector The selector to locate the form control.
 * @param errorMessage The expected error message shown in the mat-error instance.
 */
export const formFieldShouldBeInvalid = (selector: string, errorMessage: string): void => {
    cy.get(selector + ' mat-error').should(($el: JQuery) => expect($el.text().trim()).to.equal(errorMessage));
};

/**
 * Validates that an error is shown on the form control for the given input values.
 * @example
 * allFormFieldsShouldBeValidForInput({ name: toControlSelector(name) }, {
 *     name: {
 *         'This field is mandatory.': ['']
 *     }
 * });
 * @param selectors The selectors to locate the form controls.
 * @param expectedErrors The expected errors for each form control, together with the values that should trigger the error.
 */
export const allFormFieldsShouldBeInvalidForInput = (
    selectors: Record<string, string>,
    expectedErrors: ExpectedFormControlErrors
): void => {
    Object.keys(expectedErrors).forEach((controlName: string) => {
        const controlErrors: Record<string, Array<string>> = notUndefined(expectedErrors[controlName]);
        Object.keys(controlErrors).forEach((errorMessage: string) => {
            const inputValuesThatHaveThisError: Array<string> = notUndefined(controlErrors[errorMessage]);
            inputValuesThatHaveThisError.forEach((inputValue: string) => {
                setFormFieldValues({ [controlName]: inputValue }, selectors);
                formFieldShouldBeInvalid(notUndefined(selectors[controlName]), errorMessage);
            });
        });
    });
};
