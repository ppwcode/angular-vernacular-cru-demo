/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { clearText, enterText, shouldHaveValue } from './input';

/**
 * Sets the values for given form field names by clearing the input and entering the text afterwards.
 * @param keysAndValues The control names with their corresponding values to set.
 * @param controlSelectors The selectors to get the controls for the control names.
 */
export const setFormFieldValues = (
    keysAndValues: Record<string, string>,
    controlSelectors: Record<string, string>
): void => {
    Object.keys(keysAndValues).forEach((controlName: string) => {
        const selector: string = notUndefined(controlSelectors[controlName]);
        const value: string = notUndefined(keysAndValues[controlName]);

        clearText(`${selector} input`);
        if (value.length) {
            enterText(`${selector} input`, value);
        }
        cy.get(`${selector} input`).blur();
    });
};

/**
 * Asserts that the form matches the expected values for the given controls.
 * @param keysAndValues The control names with their expected value.
 * @param controlSelectors The selectors to get the controls for the control names.
 */
export const formShouldHaveValues = (
    keysAndValues: Record<string, string>,
    controlSelectors: Record<string, string>
): void => {
    Object.keys(keysAndValues).forEach((controlName: string) => {
        const selector: string = notUndefined(controlSelectors[controlName]);

        shouldHaveValue(`${selector} input`, notUndefined(keysAndValues[controlName]));
    });
};
