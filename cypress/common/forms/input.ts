/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Enters text in the first element it can find for the given selector.
 * The text that already exists in the element will be appended with the given text to type.
 * @param selector The selector to find the element.
 * @param textToType The text to type in the element.
 */
export const enterText = (selector: string, textToType: string): void => {
    cy.get(selector).type(textToType);
};

/**
 * Clears the text in the first element it can find for the given selector.
 * @param selector The selector to find the element.
 */
export const clearText = (selector: string): void => {
    cy.get(selector).clear();
};

/**
 * Asserts that the value for the given selector equals to the expected value.
 * @param selector The selector to find the element.
 * @param expectedValue The expected value of the element.
 */
export const shouldHaveValue = (selector: string, expectedValue: string): void => {
    cy.get(selector).should('have.value', expectedValue);
};
