<!--
  ~ Copyright 2021 – 2021 PeopleWare
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an “AS IS” BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

The search service receives terms entered by the user in a search field, and, after authorizing the user, returns a
paging list of relevant matches, in order of relevance.

The information in the object that represent a match differ per matching resource type. In all cases, a `href` property
is a relative URI that refers to the most up-to-date version of the matching resource.
