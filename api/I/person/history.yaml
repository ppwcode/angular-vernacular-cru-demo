# Copyright 2020 - 2021 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

parameters:
    - $ref: '../../common/parameters/forceError.yaml'
    - $ref: '../../common/parameters/flowId.yaml'
    - $ref: '../../common/parameters/mode.yaml'
    - $ref: '../../common/oauth2/authorization.yaml'
    - $ref: 'personId.yaml'
get:
    tags:
        - Person
    summary: History of the person's personal information changes
    description: A list of the different times at which the person's personal information was changed.
    operationId: getPersonHistory
    responses:
        '200':
            x-summary: The person exists.
            description: >-
                A list of the different times at which the person's information was changed, ordered from most to least
                recent, with links to retrieve that version of the personal information. Changes `< x-date` are
                included.


                This call does not use paging, because we expect less than 50 changes to the personal information of a
                person over the person's lifetime.
            headers:
                x-flow-id:
                    schema:
                        $ref: '../../common/id/UUID.yaml'
                x-mode:
                    schema:
                        $ref: '../../common/id/mode.yaml'
                x-date:
                    schema:
                        $ref: '../../common/resource/date.yaml'
                cache-control:
                    schema:
                        $ref: '../../common/resource/cacheControlPrivateDynamic.yaml'
            content:
                application/json:
                    schema:
                        allOf:
                            - $ref: '../../common/resource/StructureVersion1.yaml'
                            - type: object
                              required:
                                  - versions
                              properties:
                                  versions:
                                      type: array
                                      description: >-
                                          this list can never be empty; ordered from most recent to oldest
                                      minItems: 1
                                      uniqueItems: true
                                      items:
                                          allOf:
                                              - $ref: '../../common/resource/Timestamped.yaml'
                                              - type: object
                                                required:
                                                    - href
                                                properties:
                                                    href:
                                                        type: string
                                                        format: uri
                                                        minLength: 1
                                                        description:
                                                            relative URI at which this version of the person's personal
                                                            information can be retrieved (includes a query parameter
                                                            `at`, which is the same as this object's `createdAt`)
                                                        example: ..?at=2020-01-23T15:22:39.212Z
        '401':
            $ref: '../../common/oauth2/401.yaml'
        '403':
            $ref: '../../common/oauth2/403.yaml'
        '404':
            $ref: '404Get.yaml'
