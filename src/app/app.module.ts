import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { CruResourcesManagerService, PpwcodeVernacularCruModule } from '@ppwcode/ng-vernacular-cru';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { MiniPresentationsModule } from './mini-presentations/mini-presentations.module';
import { PersonsFacade } from './persons/persons.facade';
import { PersonsState } from './persons/persons.state';
import { StocksFacade } from './stocks/stocks.facade';
import { StocksState } from './stocks/stocks.state';

export const createTranslateLoader = (http: HttpClient): TranslateHttpLoader =>
    new TranslateHttpLoader(http, './assets/i18n/', '.json');

@NgModule({
    declarations: [AppComponent, HomeComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        PpwcodeVernacularCruModule,
        NgxsModule.forRoot([PersonsState, StocksState]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsDataPluginModule.forRoot(),
        HttpClientModule,
        MiniPresentationsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(cruResourceManager: CruResourcesManagerService) {
        cruResourceManager.registerResource('/I/person', PersonsFacade);
        cruResourceManager.registerResource('/I/stock', StocksFacade);
    }
}
