/* eslint-disable max-lines-per-function */
/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Type } from '@angular/core';
import { createServiceFactory, SpectatorService, SpectatorServiceFactory } from '@ngneat/spectator';
import { NgxsModule } from '@ngxs/store';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { AbstractCruState, CruStateModel, PresentationMode } from '@ppwcode/ng-vernacular-cru';

/**
 * This test suite can be used to verify the implementation of a state.
 * @param suiteName The name of the test suite. Most likely the same as the name of the state.
 * @param state The state type.
 * @param testingResource The resource instance to use during testing.
 */
export const cruStateTestSuite = <
    TResource extends Record<string, unknown>,
    TState extends AbstractCruState<TResource, CruStateModel<TResource>>
>(
    suiteName: string,
    state: Type<TState>,
    testingResource: TResource
): void => {
    describe(suiteName, () => {
        let spectator: SpectatorService<TState>;
        let resource: TResource;
        const createService: SpectatorServiceFactory<TState> = createServiceFactory({
            service: state,
            imports: [NgxsModule.forRoot([state]), NgxsDataPluginModule.forRoot()]
        });

        beforeEach(() => {
            spectator = createService();
            resource = testingResource;
        });

        it('should select a resource', () => {
            expect(spectator.service.selectedResource).toBeNull();

            spectator.service.selectResource(resource);
            expect(spectator.service.selectedResource).toBe(resource);

            spectator.service.selectResource(null);
            expect(spectator.service.selectedResource).toBeNull();
        });

        Object.values(PresentationMode)
            .filter((pm: PresentationMode) => isNaN(parseInt(pm)))
            .forEach((mode: PresentationMode) => {
                it(`should change the presentation mode to ${mode}`, () => {
                    expect(spectator.service.presentationMode).toEqual(PresentationMode.UNSET);

                    spectator.service.changePresentationMode(mode);

                    expect(spectator.service.presentationMode).toEqual(mode);
                });
            });

        [true, false].forEach((isBusy: boolean) => {
            it(`should update the busy state to ${isBusy}`, () => {
                expect(spectator.service.snapshot.isBusy).toEqual(false);

                spectator.service.setBusy(isBusy);

                expect(spectator.service.snapshot.isBusy).toEqual(isBusy);
            });
        });
    });
};
