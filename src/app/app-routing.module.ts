import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'persons',
        // eslint-disable-next-line @typescript-eslint/typedef
        loadChildren: () => import('./persons/persons.module').then((m) => m.PersonsModule)
    },
    {
        path: 'stocks',
        // eslint-disable-next-line @typescript-eslint/typedef
        loadChildren: () => import('./stocks/stocks.module').then((m) => m.StocksModule)
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'home'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, paramsInheritanceStrategy: 'always' })],
    exports: [RouterModule]
})
export class AppRoutingModule {}
