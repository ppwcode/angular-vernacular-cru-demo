import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { DynamicMiniPresentationService } from '@ppwcode/ng-vernacular-cru';

import { PersonMiniComponent } from './person-mini/person-mini.component';
import { StockMiniComponent } from './stock-mini/stock-mini.component';

@NgModule({
    declarations: [PersonMiniComponent, StockMiniComponent],
    imports: [CommonModule, MatCardModule],
    exports: [PersonMiniComponent, StockMiniComponent]
})
export class MiniPresentationsModule {
    constructor(dynamicMiniPresentationService: DynamicMiniPresentationService) {
        dynamicMiniPresentationService.registerComponents([
            ['/I/person', PersonMiniComponent],
            ['/I/stock', StockMiniComponent]
        ]);
    }
}
