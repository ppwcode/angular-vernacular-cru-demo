import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { personExamples } from '../../../../schemata/person/Person';
import { PersonMiniComponent } from './person-mini.component';
import { personFromDto } from '../../persons/interfaces/person';
import { MatCardModule } from '@angular/material/card';

describe('PersonMiniComponent', () => {
    let spectator: Spectator<PersonMiniComponent>;
    const createComponent = createComponentFactory({
        component: PersonMiniComponent,
        imports: [MatCardModule]
    });

    it('should create', () => {
        spectator = createComponent({
            props: {
                resource: personFromDto(notUndefined(personExamples[0]))
            }
        });

        expect(spectator.component).toBeTruthy();
    });
});
