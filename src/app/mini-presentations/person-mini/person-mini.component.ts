import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { Person } from '../../persons/interfaces/person';

@Component({
    selector: 'ppwcode-cru-demo-person-mini',
    templateUrl: './person-mini.component.html',
    styleUrls: ['./person-mini.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonMiniComponent implements OnChanges {
    @Input() public resource!: Person;

    public ngOnChanges(): void {
        notUndefined(this.resource);
    }
}
