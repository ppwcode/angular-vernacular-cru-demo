import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { Stock } from '../../../../schemata/stock/Stock';

@Component({
    selector: 'ppwcode-cru-demo-stock-mini',
    templateUrl: './stock-mini.component.html',
    styleUrls: ['./stock-mini.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockMiniComponent implements OnChanges {
    @Input() public resource!: Stock;

    public ngOnChanges(): void {
        notUndefined(this.resource);
    }
}
