import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { StockMiniComponent } from './stock-mini.component';
import { stockExamples } from '../../../../schemata/stock/Stock';
import { MatCardModule } from '@angular/material/card';

describe('StockMiniComponent', () => {
    let spectator: Spectator<StockMiniComponent>;
    const createComponent = createComponentFactory({
        component: StockMiniComponent,
        imports: [MatCardModule]
    });

    it('should create', () => {
        spectator = createComponent({
            props: {
                resource: notUndefined(stockExamples[0])
            }
        });

        expect(spectator.component).toBeTruthy();
    });
});
