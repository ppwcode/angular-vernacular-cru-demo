import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Event, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
    CruResourcesManagerService,
    GenericCruFacade,
    navigateToSelectedResource,
    SearchResult,
    SearchResults,
    SearchService
} from '@ppwcode/ng-vernacular-cru';
import { Observable, of, Subject } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { Person as personSchema } from '../../schemata/person/Person';
import { Stock } from '../../schemata/stock/Stock';
import { Person, personFromDto } from './persons/interfaces/person';

declare type SearchResultTypes = Person | Stock;
// TODO (glenstaes): Find a way to do this differently.
// Thoughts: Mapper of getById of the facade? This will most likely be the same I guess.
export const SEARCH_RESULT_MAPPERS: { [key: string]: (resource: unknown) => SearchResultTypes } = {
    '/I/person': (resource: unknown) => personFromDto(resource as personSchema)
};

@Component({
    selector: 'ppwcode-cru-demo-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
    public title: string = 'cru-demo';

    public search$: Subject<string> = new Subject<string>();

    /* istanbul ignore next */
    public routerProgress$!: Observable<boolean>;

    // TODO (glenstaes): Now that this has become generic, we can probably move it into the lib in some way.
    public searchResults$: Observable<SearchResults<Record<string, unknown>>> = this.search$
        .asObservable()
        .pipe(
            switchMap((searchValue: string) =>
                searchValue.length === 0
                    ? of({ results: [], href: { first: '' } })
                    : this.searchService.search<Record<string, unknown>>(searchValue, SEARCH_RESULT_MAPPERS)
            )
        );

    constructor(
        private readonly cruResourcesManager: CruResourcesManagerService,
        private readonly searchService: SearchService,
        private readonly router: Router,
        private readonly translateService: TranslateService
    ) {}

    public ngOnInit(): void {
        this.routerProgress$ = this.router.events.pipe(
            filter((event: Event) => event instanceof NavigationStart || event instanceof NavigationEnd),
            map((event: Event) => event instanceof NavigationStart)
        );

        this.translateService.use('nl');
    }

    public async selectResource(resource: Record<string, unknown> & SearchResult): Promise<void> {
        // We haven't got a single clue which resource is selected in the search results.
        // It just gets the facade instance for the clicked type and requests navigation to that resource.
        // This means that we can navigate to any resource as long as it has been registered in the CruResourcesManagerService.
        const facade: GenericCruFacade = this.cruResourcesManager.getFacadeInstance(resource.type);
        await facade.selectResource(resource);
        await navigateToSelectedResource(this.router, facade).toPromise();
    }
}
