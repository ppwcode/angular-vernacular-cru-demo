import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { Person as personSchema, personExamples } from '../../schemata/person/Person';
import { AppComponent, SEARCH_RESULT_MAPPERS } from './app.component';
import { Person, personFromDto } from './persons/interfaces/person';
import { PersonsFacade } from './persons/persons.facade';
import {
    CruResourcesManagerService,
    PpwcodeVernacularCruModule,
    SearchResult,
    SearchService
} from '@ppwcode/ng-vernacular-cru';
import { MiniPresentationsModule } from './mini-presentations/mini-presentations.module';
import { RouterTestingModule } from '@angular/router/testing';
import { Stock, stockExamples } from '../../schemata/stock/Stock';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { Router } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { PersonsState } from './persons/persons.state';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { TranslateModule } from '@ngx-translate/core';
import { HomeComponent } from './home/home.component';

describe('AppComponent', () => {
    let spectator: Spectator<AppComponent>;
    let router: Router;
    let personsFacade: PersonsFacade;
    let searchService: SpyObject<SearchService>;
    let cruResourcesManager: SpyObject<CruResourcesManagerService>;
    const createComponent = createComponentFactory({
        component: AppComponent,
        imports: [
            HttpClientTestingModule,
            PpwcodeVernacularCruModule,
            MiniPresentationsModule,
            RouterTestingModule.withRoutes([
                {
                    path: 'persons/63663',
                    component: HomeComponent
                }
            ]),
            NgxsModule.forRoot([PersonsState]),
            NgxsDataPluginModule.forRoot(),
            TranslateModule.forRoot()
        ],
        mocks: [SearchService, CruResourcesManagerService]
    });
    const personResults: Array<Person & SearchResult> = personExamples.map((person: personSchema) => ({
        type: '/I/person',
        ...personFromDto(person)
    }));
    const stockResults: Array<Stock & SearchResult> = stockExamples.map((stock: Stock) => ({
        type: '/I/stock',
        ...stock
    }));

    const getSearchResultElements = () =>
        spectator.fixture.debugElement.nativeElement.querySelectorAll('.search-result');

    beforeEach(() => {
        spectator = createComponent({ detectChanges: false });

        personsFacade = TestBed.inject(PersonsFacade);
        spyOn(personsFacade, 'selectResource').and.callThrough();

        router = TestBed.inject(Router);
        spyOn(router, 'navigateByUrl').and.callFake(() => Promise.resolve(true));

        cruResourcesManager = spectator.inject(CruResourcesManagerService);
        cruResourcesManager.getFacadeInstance.and.returnValue(personsFacade);

        searchService = spectator.inject(SearchService);
        searchService.search.and.returnValue(of({ results: [...personResults, ...stockResults], href: { first: '' } }));

        spectator.detectChanges();
    });

    it('present the user with search results', () => {
        spectator.component.search$.next('a');
        spectator.detectChanges();
        expect(getSearchResultElements().length).toEqual(3);

        spectator.component.search$.next('');
        spectator.detectChanges();
        expect(getSearchResultElements().length).toEqual(0);
    });

    it('should select the resource when clicking on the mini presentation', () => {
        spectator.component.search$.next('a');
        spectator.detectChanges();

        const firstResult = getSearchResultElements()[0];
        firstResult.click();
        spectator.detectChanges();

        expect(personsFacade.selectResource).toHaveBeenCalledWith(notUndefined(personResults[0]));
    });

    it('should navigate when selecting a resource', async () => {
        await spectator.component.selectResource(notUndefined(personResults[0]));

        expect(router.navigateByUrl).toHaveBeenCalledWith('/persons/63663');
    });

    it('should map a person search result', () => {
        expect(notUndefined(SEARCH_RESULT_MAPPERS['/I/person'])(personExamples[0])).toEqual(
            personFromDto(notUndefined(personExamples[0]))
        );
    });
});
