import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { navigateToSelectedResource } from '@ppwcode/ng-vernacular-cru';
import Joi from 'joi';
import { first, switchMap, tap } from 'rxjs/operators';

import { Person as personSchema } from '../../../../schemata/person/Person';
import { Person } from '../interfaces/person';
import { AbstractPersonComponent } from '../person-base.component';
import { PersonsFacade } from '../persons.facade';

@Component({
    selector: 'ppwcode-cru-demo-person-update',
    templateUrl: './person-update.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonUpdateComponent extends AbstractPersonComponent {
    public validators: Joi.ObjectSchema = personSchema.tailor('update') as Joi.ObjectSchema;

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(personsFacade: PersonsFacade, private readonly router: Router) {
        // For some reason, this constructor is necessary to run the unit tests.
        super(personsFacade);
    }

    public async saveForm(): Promise<void> {
        await this.selectedPerson$
            .pipe(
                first(),
                switchMap((selectedPerson: Person | null) =>
                    this.personsFacade.upsert({
                        ...selectedPerson,
                        ...notUndefined(this.dynamicFormComponent.formGroup).value
                    })
                ),
                tap((savedPerson: Person) => this.personsFacade.selectResource(savedPerson)),
                switchMap(() => navigateToSelectedResource(this.router, this.personsFacade))
            )
            .toPromise();
    }
}
