import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createRoutingFactory, SpectatorRouting, SpyObject } from '@ngneat/spectator';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { NgxsModule } from '@ngxs/store';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { personExamples } from '../../../../schemata/person/Person';
import { Person, personFromDto } from '../interfaces/person';
import { PersonsFacade } from '../persons.facade';
import { PersonsState } from '../persons.state';
import { PersonUpdateComponent } from './person-update.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { Router } from '@angular/router';

describe('PersonUpdateComponent', () => {
    let spectator: SpectatorRouting<PersonUpdateComponent>;
    let personsFacade: SpyObject<PersonsFacade>;
    let router: SpyObject<Router>;
    let person: Person;

    const createComponent = createRoutingFactory({
        component: PersonUpdateComponent,
        imports: [
            ReactiveFormsModule,
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([PersonsState]),
            NgxsDataPluginModule.forRoot(),
            MatCardModule,
            MatIconModule,
            PpwcodeVernacularCruModule,
            PpwcodeVernacularFormsModule,
            MatFormFieldModule,
            MatInputModule,
            TranslateModule.forRoot()
        ],
        mocks: [PersonsFacade, Router]
    });

    beforeEach(() => {
        person = personFromDto(notUndefined(personExamples[0]));
        spectator = createComponent({ detectChanges: false });

        personsFacade = spectator.inject(PersonsFacade);
        personsFacade.selectedResource$ = of(person);

        router = spectator.inject(Router);
        router.navigateByUrl.and.returnValue(of(true));

        spectator.detectChanges();
    });

    it('should create', async () => {
        const { inss, dateOfBirth, pronouns, language }: Person = person;

        await expectAsync(spectator.component.selectedPerson$.pipe(first()).toPromise()).toBeResolvedTo(person);
        expect(spectator.component.dynamicFormComponent.formGroup.value).toEqual({
            inss,
            dateOfBirth,
            pronouns,
            language
        });
    });

    it('should save the person', async () => {
        personsFacade.upsert.and.returnValue(of(person));

        spectator.component.dynamicFormComponent.formGroup.patchValue({ inss: '99111201234' });
        spectator.detectChanges();

        await spectator.component.saveForm();

        expect(personsFacade.upsert).toHaveBeenCalledWith({ ...person, inss: '99111201234' });
    });
});
