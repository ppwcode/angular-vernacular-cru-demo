import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';

import { PersonReadComponent } from './person-read/person-read.component';
import { PersonUpdateComponent } from './person-update/person-update.component';
import { PersonsRoutingModule } from './persons-routing.module';

@NgModule({
    imports: [
        CommonModule,
        MatCardModule,
        PersonsRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        PpwcodeVernacularCruModule,
        PpwcodeVernacularFormsModule
    ],
    declarations: [PersonReadComponent, PersonUpdateComponent]
})
export class PersonsModule {}
