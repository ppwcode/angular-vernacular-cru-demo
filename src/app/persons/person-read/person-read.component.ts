// eslint-disable-next-line max-classes-per-file
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoricVersion, mixinHandleSubscriptions } from '@ppwcode/ng-vernacular-cru';
import { switchMap } from 'rxjs/operators';

import { PersonResolver } from '../person.resolver';
import { AbstractPersonComponent } from '../person-base.component';
import { PersonsFacade } from '../persons.facade';

class PersonReadComponentBase extends AbstractPersonComponent {}

@Component({
    selector: 'ppwcode-cru-demo-person-read',
    templateUrl: './person-read.component.html',
    styleUrls: ['./person-read.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonReadComponent extends mixinHandleSubscriptions(PersonReadComponentBase) implements OnInit {
    constructor(
        personsFacade: PersonsFacade,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly personResolver: PersonResolver
    ) {
        super(personsFacade);
    }

    public override ngOnInit(): void {
        super.ngOnInit();

        this.stopOnDestroy(this.activatedRoute.queryParams)
            .pipe(switchMap(() => this.personResolver.resolve(this.activatedRoute.snapshot)))
            .subscribe();
    }

    public async onVersionSelected(version: HistoricVersion): Promise<void> {
        await this.router.navigate([], {
            queryParams: { at: version.createdAt },
            relativeTo: this.activatedRoute
        });
    }
}
