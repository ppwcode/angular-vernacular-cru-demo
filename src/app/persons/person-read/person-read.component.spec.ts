import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { NgxsModule } from '@ngxs/store';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { personExamples } from '../../../../schemata/person/Person';
import { PersonsState } from '../persons.state';
import { PersonsFacade } from '../persons.facade';
import { Person, personFromDto } from '../interfaces/person';
import { PersonReadComponent } from './person-read.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { PersonResolver } from '../person.resolver';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep } from 'lodash';

describe('PersonReadComponent', () => {
    let spectator: Spectator<PersonReadComponent>;
    let personsFacade: SpyObject<PersonsFacade>;
    let personResolver: SpyObject<PersonResolver>;
    let router: SpyObject<Router>;
    let activatedRoute: SpyObject<ActivatedRoute>;
    let person: Person;

    const createComponent = createComponentFactory({
        component: PersonReadComponent,
        imports: [
            ReactiveFormsModule,
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([PersonsState]),
            NgxsDataPluginModule.forRoot(),
            MatCardModule,
            MatIconModule,
            PpwcodeVernacularCruModule,
            PpwcodeVernacularFormsModule,
            MatFormFieldModule,
            MatInputModule,
            TranslateModule.forRoot()
        ],
        mocks: [PersonResolver, Router],
        providers: [{ provide: ActivatedRoute, useValue: { queryParams: of({}) } }]
    });

    beforeEach(() => {
        person = personFromDto(notUndefined(personExamples[0]));
        spectator = createComponent({ detectChanges: false });

        personsFacade = spectator.inject(PersonsFacade);
        personsFacade.selectedResource$ = of(person);
        personsFacade.selectedResourceHistory$ = of([{ createdAt: person._audit.createdAt, href: 'hrefvalue' }]);

        personResolver = spectator.inject(PersonResolver);
        personResolver.resolve.and.returnValue(of(cloneDeep(person)));

        router = spectator.inject(Router);
        router.navigate.and.returnValue(Promise.resolve());

        activatedRoute = spectator.inject(ActivatedRoute);

        spectator.detectChanges();
    });

    it('should create', async () => {
        const { inss, dateOfBirth, pronouns, language }: Person = person;

        await expectAsync(spectator.component.selectedPerson$.pipe(first()).toPromise()).toBeResolvedTo(person);
        await expectAsync(spectator.component.isLatestVersion$.pipe(first()).toPromise()).toBeResolvedTo(true);
        expect(spectator.component.dynamicFormComponent.formGroup.value).toEqual({
            inss,
            dateOfBirth,
            pronouns,
            language
        });
    });

    it('should update the timestamp in the router when changing historic version', async () => {
        const now: string = new Date().toISOString();
        await spectator.component.onVersionSelected({ createdAt: now, href: 'hrefvalue' });

        expect(router.navigate).toHaveBeenCalledWith([], {
            queryParams: { at: now },
            relativeTo: activatedRoute
        });
    });
});
