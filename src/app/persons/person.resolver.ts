import { Injectable } from '@angular/core';
import { AbstractCruResolver } from '@ppwcode/ng-vernacular-cru';

import { Person as personSchema } from '../../../schemata/person/Person';
import { Person } from './interfaces/person';
import { PersonsFacade } from './persons.facade';

@Injectable({
    providedIn: 'root'
})
export class PersonResolver extends AbstractCruResolver<Person, personSchema> {
    public routeParamName: string = 'id';

    constructor(public facade: PersonsFacade) {
        super();
    }
}
