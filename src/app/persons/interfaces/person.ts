import { Gender } from '../../../../schemata/human/Gender';
import { Address } from '../../../../schemata/location/Address';
import { Person as personSchema } from '../../../../schemata/person/Person';

export interface Person extends Record<string, unknown> {
    id?: number;
    inss: string;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
    pronouns: Gender;
    language: string;
    address: Address;
    emails: Array<string>;
    _audit: {
        version: number;
        source: string;
        createdAt: string;
        createdBy: string;
    };
    _href: {
        history: string;
    };
}

export const personFromDto = (personDto: personSchema): Person => ({
    id: personDto.personId,
    inss: personDto.inss,
    firstName: personDto.firstName,
    lastName: personDto.lastName,
    dateOfBirth: personDto.dateOfBirth,
    pronouns: personDto.pronouns,
    language: personDto.language,
    address: personDto.address,
    emails: personDto.emails,
    _audit: {
        version: personDto.structureVersion,
        source: personDto.source,
        createdAt: personDto.createdAt,
        createdBy: personDto.createdBy
    },
    _href: {
        history: personDto.href.history
    }
});

export const personToDto = (person: Person): personSchema => ({
    personId: person.id,
    inss: person.inss,
    firstName: person.firstName,
    lastName: person.lastName,
    dateOfBirth: person.dateOfBirth,
    pronouns: person.pronouns,
    language: person.language,
    address: person.address,
    emails: person.emails,
    structureVersion: person._audit.version,
    source: person._audit.source,
    createdAt: person._audit.createdAt,
    createdBy: person._audit.createdBy,
    href: {
        history: person._href.history
    }
});
