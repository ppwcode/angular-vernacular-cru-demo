import { HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { PersonsService } from './persons.service';
import { personFromDto } from './interfaces/person';
import { HttpCallTester } from '@ppwcode/ng-testing';
import { personExamples } from '../../../schemata/person/Person';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

describe('PersonsService', () => {
    let spectator: SpectatorService<PersonsService>;
    const createService = createServiceFactory({
        service: PersonsService,
        imports: [HttpClientTestingModule]
    });

    beforeEach(() => (spectator = createService()));

    it('should get all persons', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/person`)
            .whenSubscribingTo(spectator.service.getAll())
            .withResponse(personExamples)
            .expectStreamResultTo((result) => {
                expect(result).toEqual(personExamples.map(personFromDto));
            })
            .verify();
    });

    it('should get a person by its unique identifier', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/person/63663`)
            .whenSubscribingTo(spectator.service.getByIdentifier('63663'))
            .withResponse(personExamples[0])
            .expectStreamResultTo((result) => {
                expect(result).toEqual(personFromDto(notUndefined(personExamples[0])));
            })
            .verify();
    });

    it('should create a person', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/person`)
            .whenSubscribingTo(spectator.service.create(notUndefined(personExamples[0])))
            .withResponse(personExamples[0])
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('POST');
                expect(request.request.body).toEqual(personExamples[0]);
            })
            .verify();
    });

    it('should update a person', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/person/63663`)
            .whenSubscribingTo(spectator.service.update(notUndefined(personExamples[0])))
            .withResponse(personExamples[0])
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('PUT');
                expect(request.request.body).toEqual(personExamples[0]);
            })
            .verify();
    });
});
