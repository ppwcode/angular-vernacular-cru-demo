import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { generateCruRoutes } from '@ppwcode/ng-vernacular-cru';

import { PersonResolver } from './person.resolver';
import { PersonReadComponent } from './person-read/person-read.component';
import { PersonUpdateComponent } from './person-update/person-update.component';

const routes: Routes = generateCruRoutes(
    '/I/person',
    {
        components: {
            create: null,
            read: PersonReadComponent,
            update: PersonUpdateComponent
        },
        resourceRouteParamName: ':id'
    },
    PersonResolver
);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PersonsRoutingModule {}
