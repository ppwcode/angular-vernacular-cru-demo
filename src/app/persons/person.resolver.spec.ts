import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { cruResolverTestSuite } from '@ppwcode/ng-vernacular-cru';

import { Person as personSchema, personExamples } from '../../../schemata/person/Person';
import { Person, personFromDto } from './interfaces/person';
import { PersonsFacade } from './persons.facade';
import { PersonResolver } from './person.resolver';

cruResolverTestSuite<Person, PersonResolver, personSchema>(
    'PersonResolver',
    {
        Facade: PersonsFacade,
        Resolver: PersonResolver
    },
    personFromDto(notUndefined(personExamples[0]))
);
