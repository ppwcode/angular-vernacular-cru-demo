import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { AbstractCruFacade } from '@ppwcode/ng-vernacular-cru';

import { Person as personSchema } from '../../../schemata/person/Person';
import { Person, personToDto } from './interfaces/person';
import { PersonsService } from './persons.service';
import { PersonsState, PersonsStateModel } from './persons.state';

@Injectable({ providedIn: 'root' })
export class PersonsFacade extends AbstractCruFacade<Person, PersonsStateModel, personSchema> {
    public readonly resourceAppPath: string = '/persons';

    constructor(store: Store, personsState: PersonsState, personsService: PersonsService) {
        super(store, personsState, personsService);
        this.setSelectors(PersonsState);
    }

    public getResourceUniqueIdentifier(resource: Person): string | undefined {
        return resource.id?.toString();
    }

    public getResourceDisplayText(resource: Person): string {
        return `${resource.firstName} ${resource.lastName}`;
    }

    public getResourceSummaryText(resource: Person): string {
        return `${resource.address.postalCode} ${resource.address.municipality}`;
    }

    public getResourceHistoryPath(resource: Person): string | null {
        return `/api/I/person/${resource._href.history}`;
    }

    public override resourceToDto(resource: Person): personSchema {
        return personToDto(resource);
    }
}
