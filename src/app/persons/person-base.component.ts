/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Directive, OnInit, ViewChild } from '@angular/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import {
    AbstractControlGroup,
    DynamicFormComponent,
    HistoricVersion,
    TextControl,
    VerticalControls
} from '@ppwcode/ng-vernacular-cru';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Person } from './interfaces/person';
import { PersonsFacade } from './persons.facade';

@Directive()
export abstract class AbstractPersonComponent implements OnInit {
    @ViewChild(DynamicFormComponent) public dynamicFormComponent!: DynamicFormComponent;

    public selectedPerson$!: Observable<Person | null>;
    public selectedPersonHistory$!: Observable<Array<HistoricVersion> | null>;
    public isLatestVersion$!: Observable<boolean>;

    public controlGroups: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('inss', { label: 'INSS' }),
            new TextControl('dateOfBirth', { label: 'Date of Birth', inputType: 'date' }),
            new TextControl('pronouns', { label: 'Pronouns' }),
            new TextControl('language', { label: 'Language' })
        ])
    ];

    public get isBusy$(): Observable<boolean> {
        return this.personsFacade.isBusy$;
    }

    constructor(protected readonly personsFacade: PersonsFacade) {}

    public ngOnInit(): void {
        this.selectedPerson$ = this.personsFacade.selectedResource$;
        this.selectedPersonHistory$ = this.personsFacade.selectedResourceHistory$;
        this.isLatestVersion$ = combineLatest([this.selectedPerson$, this.selectedPersonHistory$]).pipe(
            map(
                ([person, history]: [Person | null, Array<HistoricVersion> | null]) =>
                    person !== null &&
                    history !== null &&
                    person._audit.createdAt === notUndefined(history[0]).createdAt
            )
        );
    }
}
