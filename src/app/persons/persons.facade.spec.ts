import { createServiceFactory, SpectatorService, SpyObject } from '@ngneat/spectator';
import { PersonsService } from './persons.service';
import { PersonsFacade } from './persons.facade';
import { NgxsModule } from '@ngxs/store';
import { PersonsState } from './persons.state';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { TestBed } from '@angular/core/testing';
import { first } from 'rxjs/operators';
import { Person, personFromDto, personToDto } from './interfaces/person';
import { personExamples } from '../../../schemata/person/Person';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('PersonsFacade', () => {
    let spectator: SpectatorService<PersonsFacade>;
    let personsService: SpyObject<PersonsService>;
    let personsState: PersonsState;
    let person: Person;

    const createService = createServiceFactory({
        service: PersonsFacade,
        mocks: [PersonsService],
        imports: [NgxsModule.forRoot([PersonsState]), NgxsDataPluginModule.forRoot(), RouterTestingModule]
    });

    beforeEach(() => {
        person = personFromDto(notUndefined(personExamples[0]));

        spectator = createService();
        personsService = spectator.inject(PersonsService);
        personsState = TestBed.inject(PersonsState);
    });

    afterEach(() => personsState.reset());

    it('should get all persons', () => {
        spectator.service.getAll();
        expect(personsService.getAll).toHaveBeenCalled();
    });

    it('should get a single person by its id', async () => {
        spyOn(personsState, 'setBusy');
        personsService.getByIdentifier.and.returnValue(of(person));
        personsService.getHistory.and.returnValue(of([]));

        await spectator.service.getByIdentifier('1').pipe(first()).toPromise();

        expect(personsService.getByIdentifier).toHaveBeenCalledWith('1', undefined);
        expect(personsState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should create a person if there is no id given', async () => {
        spyOn(personsState, 'setBusy');

        const newPerson: Person = { ...person, id: undefined };
        personsService.create.and.returnValue(of(newPerson));

        await spectator.service.upsert(newPerson).pipe(first()).toPromise();

        expect(personsService.create).toHaveBeenCalledWith(personToDto(newPerson));
        expect(personsState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should update a person if there is an existing id for it', async () => {
        spyOn(personsState, 'setBusy');

        personsService.update.and.returnValue(of(person));

        await spectator.service.upsert(person).pipe(first()).toPromise();

        expect(personsService.update).toHaveBeenCalledWith(personToDto(person));
        expect(personsState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should select a person', async () => {
        spyOn(personsState, 'selectResource').and.callThrough();
        expect(personsState.selectedResource).toBeNull();
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(null);

        spectator.service.selectResource(person);

        expect(personsState.selectedResource).toBe(person);
        expect(personsState.selectResource).toHaveBeenCalledTimes(1);
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(person);

        spectator.service.selectResource(person);

        // It should not have updated the state.
        expect(personsState.selectResource).toHaveBeenCalledTimes(1);
    });

    it('should get the display text for a person', () => {
        expect(spectator.service.getResourceDisplayText(person)).toEqual('Jane Doe');
    });

    it('should get the summary text for a person', () => {
        expect(spectator.service.getResourceSummaryText(person)).toEqual('2500 Lier');
    });

    it('should get the history path for a person', () => {
        expect(spectator.service.getResourceHistoryPath(person)).toEqual('/api/I/person/63663/history');
    });
});
