import { Injectable } from '@angular/core';
import { State } from '@ngxs/store';
import { StateRepository } from '@ngxs-labs/data/decorators';
import { AbstractCruState, CruStateModel, getDefaultCruState } from '@ppwcode/ng-vernacular-cru';

import { Person } from './interfaces/person';

export declare type PersonsStateModel = CruStateModel<Person>;

@StateRepository()
@State<PersonsStateModel>({
    name: 'persons',
    defaults: getDefaultCruState<Person>()
})
@Injectable()
export class PersonsState extends AbstractCruState<Person, PersonsStateModel> {}
