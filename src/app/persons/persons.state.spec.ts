import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { personExamples } from '../../../schemata/person/Person';
import { cruStateTestSuite } from '../lib/+state/testing/cru-state-test-suite';
import { personFromDto } from './interfaces/person';
import { PersonsState } from './persons.state';

cruStateTestSuite('PersonsState', PersonsState, personFromDto(notUndefined(personExamples[0])));
