import { Injectable } from '@angular/core';
import { HttpMethod } from '@angular-ru/cdk/http/typings';
import { AbstractCruService, Endpoint, EndpointService } from '@ppwcode/ng-vernacular-cru';
import Joi from 'joi';

import { Person as personSchema } from '../../../schemata/person/Person';
import { Person, personFromDto } from './interfaces/person';

@Injectable({
    providedIn: 'root'
})
export class PersonsService extends AbstractCruService<Person, personSchema> {
    public endpoints: {
        getAll: Endpoint<Array<personSchema>, Array<Person>>;
        getById: Endpoint<personSchema, Person, { identifier: string }>;
        create: Endpoint<personSchema, Person, personSchema>;
        update: Endpoint<personSchema, Person, personSchema>;
    } = {
        getAll: new Endpoint<Array<personSchema>, Array<Person>>(
            HttpMethod.GET,
            '/api/I/person',
            null,
            Joi.array().items(personSchema.tailor('read')),
            (persons: Array<personSchema>) => persons.map(personFromDto)
        ),
        getById: new Endpoint<personSchema, Person, { identifier: string }>(
            HttpMethod.GET,
            (context: { identifier: string }) => `/api/I/person/${context.identifier}`,
            null,
            personSchema.tailor('read'),
            personFromDto
        ),
        create: new Endpoint<personSchema, Person, personSchema>(
            HttpMethod.POST,
            `/api/I/person`,
            personSchema.tailor('create'),
            personSchema.tailor('read'),
            personFromDto
        ),
        update: new Endpoint<personSchema, Person, personSchema>(
            HttpMethod.PUT,
            (personDto: personSchema) => `/api/I/person/${personDto.personId}`,
            personSchema.tailor('update'),
            personSchema.tailor('read'),
            personFromDto
        )
    };

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(endpointService: EndpointService) {
        // Constructor is not useless because we are extending an abstract class that can't be injected to.
        super(endpointService);
    }
}
