import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';

import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockReadComponent } from './stock-read/stock-read.component';
import { StockUpdateComponent } from './stock-update/stock-update.component';
import { StocksRoutingModule } from './stocks-routing.module';

@NgModule({
    imports: [
        CommonModule,
        MatCardModule,
        StocksRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        PpwcodeVernacularCruModule,
        PpwcodeVernacularFormsModule,
        MatMenuModule
    ],
    declarations: [StockReadComponent, StockUpdateComponent, StockCreateComponent]
})
export class StocksModule {}
