/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { Directive, OnInit, ViewChild } from '@angular/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import {
    AbstractControlGroup,
    DynamicFormComponent,
    HistoricVersion,
    TextControl,
    VerticalControls
} from '@ppwcode/ng-vernacular-cru';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Stock } from '../../../schemata/stock/Stock';
import { StocksFacade } from './stocks.facade';

@Directive()
export abstract class AbstractStockComponent implements OnInit {
    @ViewChild(DynamicFormComponent) public dynamicFormComponent!: DynamicFormComponent;

    public selectedStock$!: Observable<Stock | null>;
    public selectedStockHistory$!: Observable<Array<HistoricVersion> | null>;
    public isLatestVersion$!: Observable<boolean>;

    public controlGroups: Array<AbstractControlGroup> = [
        new VerticalControls([
            new TextControl('symbol', { label: 'Symbol' }),
            new TextControl('name', { label: 'Name' }),
            new TextControl('address.line1', { label: 'Line 1' }),
            new TextControl('address.municipality', { label: 'Municipality' }),
            new TextControl('address.country', {
                label: 'Country',
                validationMessages: { 'joi-errors.string.pattern.base': 'stock.address.country.pattern' }
            })
        ])
    ];

    public get isBusy$(): Observable<boolean> {
        return this.stocksFacade.isBusy$;
    }

    constructor(protected readonly stocksFacade: StocksFacade) {}

    public ngOnInit(): void {
        this.selectedStock$ = this.stocksFacade.selectedResource$;
        this.selectedStockHistory$ = this.stocksFacade.selectedResourceHistory$;
        this.isLatestVersion$ = combineLatest([this.selectedStock$, this.selectedStockHistory$]).pipe(
            map(
                ([stock, history]: [Stock | null, Array<HistoricVersion> | null]) =>
                    stock !== null && history !== null && stock.createdAt === notUndefined(history[0]).createdAt
            )
        );
    }
}
