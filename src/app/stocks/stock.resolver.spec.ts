import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { cruResolverTestSuite } from '@ppwcode/ng-vernacular-cru';

import { stockExamples } from '../../../schemata/stock/Stock';
import { StocksFacade } from './stocks.facade';
import { StockResolver } from './stock.resolver';

cruResolverTestSuite(
    'StockResolver',
    {
        Facade: StocksFacade,
        Resolver: StockResolver
    },
    notUndefined(stockExamples[0])
);
