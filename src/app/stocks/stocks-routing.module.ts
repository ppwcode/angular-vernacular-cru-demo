import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { generateCruRoutes } from '@ppwcode/ng-vernacular-cru';

import { StockResolver } from './stock.resolver';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockReadComponent } from './stock-read/stock-read.component';
import { StockUpdateComponent } from './stock-update/stock-update.component';

const routes: Routes = generateCruRoutes(
    '/I/stock',
    {
        components: {
            create: StockCreateComponent,
            read: StockReadComponent,
            update: StockUpdateComponent
        },
        resourceRouteParamName: ':id'
    },
    StockResolver
);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StocksRoutingModule {}
