import { HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { HttpCallTester } from '@ppwcode/ng-testing';

import { Stock, stockExamples } from '../../../schemata/stock/Stock';
import { StocksService } from './stocks.service';

describe('StocksService', () => {
    let spectator: SpectatorService<StocksService>;
    const createService = createServiceFactory({
        service: StocksService,
        imports: [HttpClientTestingModule]
    });

    beforeEach(() => (spectator = createService()));

    it('should get all stocks', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/stock`)
            .whenSubscribingTo(spectator.service.getAll())
            .withResponse(stockExamples)
            .expectStreamResultTo((result) => {
                expect(result).toEqual(stockExamples);
            })
            .verify();
    });

    it('should get a stock by its unique identifier', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/stock/ACM`)
            .whenSubscribingTo(spectator.service.getByIdentifier('ACM'))
            .withResponse(stockExamples[0])
            .expectStreamResultTo((result) => {
                expect(result).toEqual(notUndefined(stockExamples[0]));
            })
            .verify();
    });

    it('should create a stock', () => {
        const exampleWithNewId: Stock = { ...notUndefined(stockExamples[0]), symbol: 'ACME' };
        HttpCallTester.expectOneCallToUrl(`/api/I/stock/ACME`)
            .whenSubscribingTo(spectator.service.create(exampleWithNewId))
            .withResponse(exampleWithNewId)
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('PUT');
                expect(request.request.body).toEqual(exampleWithNewId);
            })
            .verify();
    });

    it('should update a stock', () => {
        HttpCallTester.expectOneCallToUrl(`/api/I/stock/ACM`)
            .whenSubscribingTo(spectator.service.update(notUndefined(stockExamples[0])))
            .withResponse(stockExamples[0])
            .expectRequestTo((request: TestRequest) => {
                expect(request.request.method).toEqual('PUT');
                expect(request.request.body).toEqual(stockExamples[0]);
            })
            .verify();
    });
});
