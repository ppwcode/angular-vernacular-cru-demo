import { Injectable } from '@angular/core';
import { HttpMethod } from '@angular-ru/cdk/http/typings';
import { AbstractCruService, Endpoint, EndpointService } from '@ppwcode/ng-vernacular-cru';
import Joi from 'joi';

import { Stock } from '../../../schemata/stock/Stock';

@Injectable({
    providedIn: 'root'
})
export class StocksService extends AbstractCruService<Stock, Stock> {
    public endpoints: {
        getAll: Endpoint<Array<Stock>, Array<Stock>>;
        getById: Endpoint<Stock, Stock, { identifier: string }>;
        create: Endpoint<Stock, Stock, Stock>;
        update: Endpoint<Stock, Stock, Stock>;
    } = {
        getAll: new Endpoint<Array<Stock>, Array<Stock>>(
            HttpMethod.GET,
            '/api/I/stock',
            null,
            Joi.array().items(Stock.tailor('read'))
        ),
        getById: new Endpoint<Stock, Stock, { identifier: string }>(
            HttpMethod.GET,
            (context: { identifier: string }) => `/api/I/stock/${context.identifier}`,
            null,
            Stock.tailor('read')
        ),
        create: new Endpoint<Stock, Stock, Stock>(
            HttpMethod.PUT,
            (context: Stock) => `/api/I/stock/${context.symbol}`,
            Stock.tailor('create'),
            Stock.tailor('read')
        ),
        update: new Endpoint<Stock, Stock, Stock>(
            HttpMethod.PUT,
            (context: Stock) => `/api/I/stock/${context.symbol}`,
            Stock.tailor('update'),
            Stock.tailor('read')
        )
    };

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(endpointService: EndpointService) {
        // Constructor is not useless because we are extending an abstract class that can't be injected to.
        super(endpointService);
    }
}
