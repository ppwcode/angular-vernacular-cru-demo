import { Injectable } from '@angular/core';
import { State } from '@ngxs/store';
import { StateRepository } from '@ngxs-labs/data/decorators';
import { AbstractCruState, CruStateModel, getDefaultCruState } from '@ppwcode/ng-vernacular-cru';

import { Stock } from '../../../schemata/stock/Stock';

export declare type StocksStateModel = CruStateModel<Stock>;

@StateRepository()
@State<StocksStateModel>({
    name: 'stocks',
    defaults: getDefaultCruState<Stock>()
})
@Injectable()
export class StocksState extends AbstractCruState<Stock, StocksStateModel> {}
