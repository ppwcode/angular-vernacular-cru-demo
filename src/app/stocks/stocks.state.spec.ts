import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';

import { stockExamples } from '../../../schemata/stock/Stock';
import { cruStateTestSuite } from '../lib/+state/testing/cru-state-test-suite';
import { StocksState } from './stocks.state';

cruStateTestSuite('StocksState', StocksState, notUndefined(stockExamples[0]));
