import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { AbstractCruFacade } from '@ppwcode/ng-vernacular-cru';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Stock } from '../../../schemata/stock/Stock';
import { StocksService } from './stocks.service';
import { StocksState, StocksStateModel } from './stocks.state';

@Injectable({ providedIn: 'root' })
export class StocksFacade extends AbstractCruFacade<Stock, StocksStateModel, Stock> {
    public readonly resourceAppPath: string = '/stocks';

    constructor(
        private readonly stocksState: StocksState,
        private readonly stocksService: StocksService,
        store: Store
    ) {
        super(store, stocksState, stocksService);
        this.setSelectors(StocksState);
    }

    public override upsert(stock: Stock, isCreate: boolean = false): Observable<Stock> {
        this.stocksState.setBusy(true);
        let save$: Observable<Stock>;
        if (isCreate) {
            stock.structureVersion = 1;
            save$ = this.stocksService.create(stock);
        } else {
            save$ = this.stocksService.update(stock);
        }

        return save$.pipe(
            tap(() => {
                this.stocksState.setBusy(false);
            })
        );
    }

    public getResourceUniqueIdentifier(resource: Stock): string {
        return resource.symbol;
    }

    public getResourceDisplayText(resource: Stock): string {
        return resource.symbol;
    }

    public getResourceSummaryText(resource: Stock): string {
        return resource.name;
    }

    public getResourceHistoryPath(resource: Stock): string | null {
        return `/api/I/stock/${resource.href.history}`;
    }
}
