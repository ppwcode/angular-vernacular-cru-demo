import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createServiceFactory, SpectatorService, SpyObject } from '@ngneat/spectator';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { NgxsModule } from '@ngxs/store';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { Stock, stockExamples } from '../../../schemata/stock/Stock';
import { StocksFacade } from './stocks.facade';
import { StocksService } from './stocks.service';
import { StocksState } from './stocks.state';

describe('StocksFacade', () => {
    let spectator: SpectatorService<StocksFacade>;
    let stocksService: SpyObject<StocksService>;
    let stocksState: StocksState;
    let stock: Stock;

    const createService = createServiceFactory({
        service: StocksFacade,
        mocks: [StocksService],
        imports: [NgxsModule.forRoot([StocksState]), NgxsDataPluginModule.forRoot(), RouterTestingModule]
    });

    beforeEach(() => {
        stock = notUndefined(stockExamples[0]);

        spectator = createService();
        stocksService = spectator.inject(StocksService);
        stocksState = TestBed.inject(StocksState);
    });

    afterEach(() => stocksState.reset());

    it('should get whether the state is busy', async () => {
        await expectAsync(spectator.service.isBusy$.pipe(first()).toPromise()).toBeResolvedTo(false);
    });

    it('should get all stocks', () => {
        spectator.service.getAll();
        expect(stocksService.getAll).toHaveBeenCalled();
    });

    it('should get a single stock by its id', async () => {
        spyOn(stocksState, 'setBusy');
        stocksService.getByIdentifier.and.returnValue(of(stock));
        stocksService.getHistory.and.returnValue(of([]));

        await spectator.service.getByIdentifier('ACM').pipe(first()).toPromise();

        expect(stocksService.getByIdentifier).toHaveBeenCalledWith('ACM', undefined);
        expect(stocksState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should create a stock', async () => {
        spyOn(stocksState, 'setBusy');

        const newStock: Stock = { ...stock };
        stocksService.create.and.returnValue(of(newStock));

        await spectator.service.upsert(newStock, true).pipe(first()).toPromise();

        expect(stocksService.create).toHaveBeenCalledWith(newStock);
        expect(stocksState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should update a stock if there is an existing id for it', async () => {
        spyOn(stocksState, 'setBusy');

        stocksService.update.and.returnValue(of(stock));

        await spectator.service.upsert(stock).pipe(first()).toPromise();

        expect(stocksService.update).toHaveBeenCalledWith(stock);
        expect(stocksState.setBusy).toHaveBeenCalledTimes(2);
    });

    it('should select a stock', async () => {
        spyOn(stocksState, 'selectResource').and.callThrough();
        expect(stocksState.selectedResource).toBeNull();
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(null);

        spectator.service.selectResource(stock);

        expect(stocksState.selectedResource).toBe(stock);
        expect(stocksState.selectResource).toHaveBeenCalledTimes(1);
        await expectAsync(spectator.service.selectedResource$.pipe(first()).toPromise()).toBeResolvedTo(stock);

        spectator.service.selectResource(stock);

        // It should not have updated the state.
        expect(stocksState.selectResource).toHaveBeenCalledTimes(1);
    });

    it('should get the unique identifier of a stock', () => {
        expect(spectator.service.getResourceUniqueIdentifier(stock)).toEqual('ACM');
    });

    it('should get the display text for a stock', () => {
        expect(spectator.service.getResourceDisplayText(stock)).toEqual('ACM');
    });

    it('should get the summary text for a stock', () => {
        expect(spectator.service.getResourceSummaryText(stock)).toEqual('Acme');
    });

    it('should get the history path for a stock', () => {
        expect(spectator.service.getResourceHistoryPath(stock)).toEqual('/api/I/stock/KJHLGF/history');
    });
});
