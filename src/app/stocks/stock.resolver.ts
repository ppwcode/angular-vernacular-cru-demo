import { Injectable } from '@angular/core';
import { AbstractCruResolver } from '@ppwcode/ng-vernacular-cru';

import { Stock } from '../../../schemata/stock/Stock';
import { StocksFacade } from './stocks.facade';

@Injectable({
    providedIn: 'root'
})
export class StockResolver extends AbstractCruResolver<Stock> {
    public routeParamName: string = 'id';

    constructor(public facade: StocksFacade) {
        super();
    }
}
