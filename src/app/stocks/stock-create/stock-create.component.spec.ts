import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { createRoutingFactory, SpectatorRouting, SpyObject } from '@ngneat/spectator';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { NgxsModule } from '@ngxs/store';
import { TranslateModule } from '@ngx-translate/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { Stock, stockExamples } from '../../../../schemata/stock/Stock';
import { StocksFacade } from '../stocks.facade';
import { StocksState } from '../stocks.state';
import { StockCreateComponent } from './stock-create.component';

describe('StockCreateComponent', () => {
    let spectator: SpectatorRouting<StockCreateComponent>;
    let stocksFacade: SpyObject<StocksFacade>;
    let stock: Stock;

    const createComponent = createRoutingFactory({
        component: StockCreateComponent,
        imports: [
            ReactiveFormsModule,
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([StocksState]),
            NgxsDataPluginModule.forRoot(),
            MatCardModule,
            MatIconModule,
            PpwcodeVernacularCruModule,
            PpwcodeVernacularFormsModule,
            MatFormFieldModule,
            MatInputModule,
            TranslateModule.forRoot()
        ],
        mocks: [StocksFacade]
    });

    beforeEach(() => {
        stock = notUndefined(stockExamples[0]);
        spectator = createComponent({ detectChanges: false });

        stocksFacade = spectator.inject(StocksFacade);
        stocksFacade.selectedResource$ = of(null);

        spectator.detectChanges();
    });

    it('should create', async () => {
        await expectAsync(spectator.component.selectedStock$.pipe(first()).toPromise()).toBeResolvedTo(null);
        expect(spectator.component.dynamicFormComponent.formGroup.value).toEqual({
            name: null,
            symbol: null,
            address: { line1: null, municipality: null, country: null, postalCode: null }
        });
    });

    it('should save the stock', async () => {
        stocksFacade.upsert.and.returnValue(of({ ...stock }));

        spectator.component.dynamicFormComponent.formGroup.patchValue({ name: 'Acme Corp.' });
        await spectator.component.saveForm();

        expect(stocksFacade.upsert).toHaveBeenCalledWith(
            {
                name: 'Acme Corp.',
                symbol: null,
                address: { line1: null, municipality: null, country: null, postalCode: null }
            },
            true
        );
    });
});
