import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import {
    AbstractControlGroup,
    FormControlConfig,
    navigateToSelectedResource,
    TextControl
} from '@ppwcode/ng-vernacular-cru';
import Joi from 'joi';
import { switchMap, tap } from 'rxjs/operators';

import { Stock } from '../../../../schemata/stock/Stock';
import { AbstractStockComponent } from '../stock-base.component';
import { StocksFacade } from '../stocks.facade';

@Component({
    selector: 'ppwcode-cru-demo-stock-create',
    templateUrl: './stock-create.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockCreateComponent extends AbstractStockComponent {
    public validators: Joi.ObjectSchema = Stock.tailor('create') as Joi.ObjectSchema;

    constructor(stocksFacade: StocksFacade, private readonly router: Router) {
        super(stocksFacade);

        const addressGroup: AbstractControlGroup = notUndefined(this.controlGroups[0]);
        addressGroup.controls.splice(
            addressGroup.controls.findIndex((c: FormControlConfig) => c.name === 'address.municipality'),
            0,
            new TextControl('address.postalCode', { label: 'Postal Code' })
        );
    }

    public async saveForm(): Promise<void> {
        await this.stocksFacade
            .upsert(
                {
                    ...notUndefined(this.dynamicFormComponent.formGroup).value
                },
                true
            )
            .pipe(
                tap((savedStock: Stock) => this.stocksFacade.selectResource(savedStock)),
                switchMap(() => navigateToSelectedResource(this.router, this.stocksFacade))
            )
            .toPromise();
    }
}
