import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { createRoutingFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { NgxsModule } from '@ngxs/store';
import { TranslateModule } from '@ngx-translate/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { Address } from '../../../../schemata/location/Address';
import { Stock, stockExamples } from '../../../../schemata/stock/Stock';
import { StocksFacade } from '../stocks.facade';
import { StocksState } from '../stocks.state';
import { StockUpdateComponent } from './stock-update.component';
import { Router } from '@angular/router';

describe('StockUpdateComponent', () => {
    let spectator: Spectator<StockUpdateComponent>;
    let stocksFacade: SpyObject<StocksFacade>;
    let router: SpyObject<Router>;
    let stock: Stock;

    const createComponent = createRoutingFactory({
        component: StockUpdateComponent,
        imports: [
            ReactiveFormsModule,
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([StocksState]),
            NgxsDataPluginModule.forRoot(),
            MatCardModule,
            MatIconModule,
            PpwcodeVernacularCruModule,
            PpwcodeVernacularFormsModule,
            MatFormFieldModule,
            MatInputModule,
            TranslateModule.forRoot()
        ],
        mocks: [StocksFacade, Router]
    });

    beforeEach(() => {
        stock = notUndefined(stockExamples[0]);
        spectator = createComponent({ detectChanges: false });

        stocksFacade = spectator.inject(StocksFacade);
        stocksFacade.selectedResource$ = of(cloneDeep(stock));

        router = spectator.inject(Router);
        router.navigateByUrl.and.returnValue(of(true));

        spectator.detectChanges();
    });

    it('should create', async () => {
        const { name, address, symbol }: Stock = stock;

        const partialAddress: Partial<Address> = { ...address };
        delete partialAddress.postalCode;

        await expectAsync(spectator.component.selectedStock$.pipe(first()).toPromise()).toBeResolvedTo(stock);
        expect(spectator.component.dynamicFormComponent.formGroup.value).toEqual({
            name,
            address: partialAddress,
            symbol
        });
    });

    it('should save the stock', async () => {
        stocksFacade.upsert.and.returnValue(of({ ...stock }));

        spectator.component.dynamicFormComponent.formGroup.patchValue({
            name: 'Acme Corp.'
        });
        await spectator.component.saveForm();

        expect(stocksFacade.upsert).toHaveBeenCalledWith({ ...stock, name: 'Acme Corp.' });
    });
});
