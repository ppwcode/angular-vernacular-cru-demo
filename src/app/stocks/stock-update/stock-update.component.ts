import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { navigateToSelectedResource } from '@ppwcode/ng-vernacular-cru';
import Joi from 'joi';
import { merge } from 'lodash';
import { first, switchMap, tap } from 'rxjs/operators';

import { Stock } from '../../../../schemata/stock/Stock';
import { AbstractStockComponent } from '../stock-base.component';
import { StocksFacade } from '../stocks.facade';

@Component({
    selector: 'ppwcode-cru-demo-stock-update',
    templateUrl: './stock-update.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockUpdateComponent extends AbstractStockComponent {
    public validators: Joi.ObjectSchema = Stock.tailor('update') as Joi.ObjectSchema;

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(stocksFacade: StocksFacade, private readonly router: Router) {
        // For some reason, this constructor is necessary to run the unit tests.
        super(stocksFacade);
    }

    public async saveForm(): Promise<void> {
        await this.selectedStock$
            .pipe(
                first(),
                switchMap((selectedStock: Stock | null) =>
                    this.stocksFacade.upsert(
                        merge({ ...selectedStock } as Stock, {
                            ...notUndefined(this.dynamicFormComponent.formGroup).value
                        })
                    )
                ),
                tap((savedStock: Stock) => this.stocksFacade.selectResource(savedStock)),
                switchMap(() => navigateToSelectedResource(this.router, this.stocksFacade))
            )
            .toPromise();
    }
}
