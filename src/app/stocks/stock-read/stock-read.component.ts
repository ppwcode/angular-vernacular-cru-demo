// eslint-disable-next-line max-classes-per-file
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    AbstractControlGroup,
    FormControlConfig,
    HistoricVersion,
    mixinHandleSubscriptions
} from '@ppwcode/ng-vernacular-cru';
import { switchMap } from 'rxjs/operators';

import { StockResolver } from '../stock.resolver';
import { AbstractStockComponent } from '../stock-base.component';
import { StocksFacade } from '../stocks.facade';

// Boilerplate to be able to apply the mixins to the abstract component.
class StockReadComponentBase extends AbstractStockComponent {}

@Component({
    selector: 'ppwcode-cru-demo-stock-read',
    templateUrl: './stock-read.component.html',
    styleUrls: ['./stock-read.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockReadComponent extends mixinHandleSubscriptions(StockReadComponentBase) implements OnInit {
    constructor(
        stocksFacade: StocksFacade,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly stockResolver: StockResolver
    ) {
        super(stocksFacade);
        this.controlGroups = this.controlGroups.map((group: AbstractControlGroup) => {
            group.controls = group.controls.filter((control: FormControlConfig) => control.name !== 'symbol');
            return group;
        });
    }

    public override ngOnInit(): void {
        super.ngOnInit();

        this.stopOnDestroy(this.activatedRoute.queryParams)
            .pipe(switchMap(() => this.stockResolver.resolve(this.activatedRoute.snapshot)))
            .subscribe();
    }

    public async onVersionSelected(version: HistoricVersion): Promise<void> {
        await this.router.navigate([], {
            queryParams: { at: version.createdAt },
            relativeTo: this.activatedRoute
        });
    }
}
