import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import { NgxsModule } from '@ngxs/store';
import { NgxsDataPluginModule } from '@ngxs-labs/data';
import { TranslateModule } from '@ngx-translate/core';
import { notUndefined } from '@ppwcode/js-ts-oddsandends/lib/conditional-assert';
import { PpwcodeVernacularCruModule, PpwcodeVernacularFormsModule } from '@ppwcode/ng-vernacular-cru';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { Address } from '../../../../schemata/location/Address';
import { Stock, stockExamples } from '../../../../schemata/stock/Stock';
import { StocksFacade } from '../stocks.facade';
import { StocksState } from '../stocks.state';
import { StockReadComponent } from './stock-read.component';
import { MatMenuModule } from '@angular/material/menu';
import { StockResolver } from '../stock.resolver';
import { ActivatedRoute, Router } from '@angular/router';

describe('StockReadComponent', () => {
    let spectator: Spectator<StockReadComponent>;
    let stocksFacade: SpyObject<StocksFacade>;
    let stockResolver: SpyObject<StockResolver>;
    let router: SpyObject<Router>;
    let activatedRoute: SpyObject<ActivatedRoute>;
    let stock: Stock;

    const createComponent = createComponentFactory({
        component: StockReadComponent,
        imports: [
            ReactiveFormsModule,
            RouterTestingModule,
            HttpClientTestingModule,
            NgxsModule.forRoot([StocksState]),
            NgxsDataPluginModule.forRoot(),
            MatCardModule,
            MatIconModule,
            PpwcodeVernacularCruModule,
            PpwcodeVernacularFormsModule,
            MatFormFieldModule,
            MatMenuModule,
            MatInputModule,
            TranslateModule.forRoot()
        ],
        mocks: [StocksFacade, StockResolver, Router],
        providers: [{ provide: ActivatedRoute, useValue: { queryParams: of({}) } }]
    });

    beforeEach(() => {
        stock = notUndefined(stockExamples[0]);
        spectator = createComponent({ detectChanges: false });

        stocksFacade = spectator.inject(StocksFacade);
        stocksFacade.selectedResource$ = of(cloneDeep(stock));
        stocksFacade.selectedResourceHistory$ = of([{ createdAt: stock.createdAt, href: stock.symbol }]);

        stockResolver = spectator.inject(StockResolver);
        stockResolver.resolve.and.returnValue(of(cloneDeep(stock)));

        router = spectator.inject(Router);
        router.navigate.and.returnValue(Promise.resolve());

        activatedRoute = spectator.inject(ActivatedRoute);

        spectator.detectChanges();
    });

    it('should create', async () => {
        const { name, address }: Stock = stock;

        const partialAddress: Partial<Address> = { ...address };
        delete partialAddress.postalCode;

        await expectAsync(spectator.component.selectedStock$.pipe(first()).toPromise()).toBeResolvedTo(stock);
        await expectAsync(spectator.component.isLatestVersion$.pipe(first()).toPromise()).toBeResolvedTo(true);
        expect(spectator.component.dynamicFormComponent.formGroup.value).toEqual({
            name,
            address: partialAddress
        });
    });

    it('should update the timestamp in the router when changing historic version', async () => {
        const now: string = new Date().toISOString();
        await spectator.component.onVersionSelected({ createdAt: now, href: 'hrefvalue' });

        expect(router.navigate).toHaveBeenCalledWith([], {
            queryParams: { at: now },
            relativeTo: activatedRoute
        });
    });
});
